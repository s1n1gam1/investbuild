//Script for about slider in index page******************************************************************
jQuery(document).ready(function($) {

		let counterIndex=1;

    	loadAboutSlider(counterIndex);

    	$('.main-about__controller-wrap').click(function(event) {
            let sliderLength= this.dataset.count;
    		let id = $(this).attr('id');

    		if(id=="index_slider_next"){
    			if(counterIndex == sliderLength){
    				counterIndex=1;
    			}
    			else{
					counterIndex+=1;
    			}
    		}
    		else if(id=="index_slider_prev"){
				if(counterIndex==1){
    				counterIndex=sliderLength;
    			}
    			else{
                    counterIndex-=1;
    			}
    		}

            if(counterIndex==1){
               // $(".main-about__left").removeClass('resetAboutLeft');
                $(".main-about__slider-wrapper").removeClass('resetSliderWrapper');
            }
            else{
               // $(".main-about__left").addClass('resetAboutLeft');
                $(".main-about__slider-wrapper").addClass('resetSliderWrapper');                
            }

    		loadAboutSlider(counterIndex);
    	});


	function loadAboutSlider(counter) {

    	$('.main-about__text-wrapper').removeClass('active')
    	$('.main-about__slider-wrapper__image').removeClass('active')
    	$('.main-about__dots').removeClass('active')

    	$('#index_slider_image_'+counter).addClass('active')
    	$('#index_slider_text_'+counter).addClass('active')
    	$('#index_slider_dot_'+counter).addClass('active')


    }
});
//Script for objects slider in index page******************************************************************
jQuery(document).ready(function($) {
   let counterIndex=1;
        let sliderLength= $('.main-objects__left').data('count');
        loadObjectSlider(counterIndex);

        $('.main-objects__controller-wrap').click(function(event) {
            let id = $(this).attr('id');

            if(id=="index_object_next"){
                if(counterIndex==sliderLength){
                    counterIndex=1;
                }
                else{
                    counterIndex+=1;
                }
            }
            else if(id=="index_object_prev"){
                if(counterIndex==1){
                    counterIndex=sliderLength;
                }
                else{
                    counterIndex-=1;
                }
            }

            loadObjectSlider(counterIndex);
        });


    function loadObjectSlider(counter) {
        $('.main-objects__text-wrap').removeClass('active')
        $('.main-objects__image').removeClass('active')

        $('#index_objects_text_'+counter).addClass('active')
        $('#index_object_image_'+counter).addClass('active')


    }


});
//Script for about page tab blocks******************************************************************
jQuery(document).ready(function($) {
   let counterIndex=1;
        let sliderLength= $('.slider-about__text-block').data('count');
        directionTabsLoad(counterIndex);

        $('.slider-about__arrows-img').click(function(event) {
            let id = $(this).attr('id');

            if(id=="slider_about_right"){
                if(counterIndex==sliderLength){
                    counterIndex=1;
                }
                else{
                    counterIndex+=1;
                }
            }
            else if(id=="slider_about_left"){
                if(counterIndex==1){
                    counterIndex=sliderLength;
                }
                else{
                    counterIndex-=1;
                }
            }

            $('.service-types-tabs__wrapper .active-item').removeClass('selected')
           $('#service-types-tabs-'+counterIndex).addClass('selected');

            directionTabsLoad(counterIndex);
        });

        $('.service-types-tabs__wrapper').click(function(event) {
           let id  = $(this).data('slide-id');

           $('.service-types-tabs__wrapper .active-item').removeClass('selected')
           $('#service-types-tabs-'+id).addClass('selected');

           directionTabsLoad(id);
        });


    function directionTabsLoad(counter) {
        $('.slider-about__text-block-wrap').removeClass('active')
        $('.slider-about__tab-image').removeClass('active')

        $('#slider_about_text_'+counter).addClass('active')
        $('#slider-about-image-'+counter).addClass('active')


    }


});

