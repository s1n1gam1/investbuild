

var coords = [
    {
        "title": "ЖК «Авиценна-2»",
        "coordinates": [51.113873, 71.434700]
    },

    {
        "title": "ЖК «Авиценна»",
        "coordinates": [51.134730, 71.430564]
    },

    {
        "title": "ЖК «Абай на Отырар» - Габдуллина",
        "coordinates": [51.167860, 71.432226]
    },

    {
        "title": "ЖК «Абай-2»",
        "coordinates": [51.168537, 71.440347]
    },

    {
        "title": "ЖК «Авиценна-Элит»",
        "coordinates": [51.133452, 71.433313]
    },

    {
        "title": "ЖК «Иманова»",
        "coordinates": [51.162969, 71.437265]
    },

];

function init() {
    var myMap = new ymaps.Map("map", {
        center: [51.130690,71.475368 ],
        zoom: 12
    }, {
        searchControlProvider: 'yandex#search'
    });

    myCollection = new ymaps.GeoObjectCollection(null, {
        preset: 'islands#blueIcon'
    });

    for (var i = 0, l = coords.length; i < l; i++) {
        myCollection.add(new ymaps.Placemark(coords[i].coordinates
            , {
                balloonContent: '<strong>+coords[i].title+</strong>'
            }, {
                preset: 'islands#icon',
                iconColor: '#0095b6'
            }
            ));
    }

    myMap.geoObjects.add(myCollection);


    $('#setObject1').click(function(event) {
        myMap.panTo([51.113873, 71.434700],{
            flying:1,
        });
    });

    $('#setObject2').click(function(event) {
        myMap.panTo([51.134730, 71.430564],{
            flying:1,
        });
    });

    $('#setObject3').click(function(event) {
        myMap.panTo([51.167860, 71.432226],{
            flying:1,
        });
    });

    $('#setObject4').click(function(event) {
        myMap.panTo([51.168537, 71.440347],{
            flying:1,
        });
    });

    $('#setObject5').click(function(event) {
        myMap.panTo([51.133452, 71.433313],{
            flying:1,
        });
    });

    $('#setObject6').click(function(event) {
        myMap.panTo([51.162969, 71.437265],{
            flying:1,
        });
    });
}

ymaps.ready(init);