<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Основные страницы

Route::get('/', function () {
    $sliders = \App\Modules\Slider\Models\Slider::whereStatusId(\App\Modules\Slider\Models\Slider::STATUS_ACTIVE)->get();
    return view('pages.main', compact('sliders'));
});

Route::post('feedback', 'MainController@feedback')->name('feedback');


Route::get('about', function () {
    return view('pages.about');
})->name('about');



Route::get('contacts', function () {
    return view('pages.contacts');
});


Route::get('gallery-detail', function () {
    return view('pages.gallery-detail');
});


Route::get('partners', function () {
    return view('pages.partners');
});

Route::get('build-materials', function () {
    return view('pages.build-materials');
});




//Стадартные страницы
Route::get('error', function () {
    return view('pages.page-not-found');
});


