@extends('layouts.app')

@section('title', 'Главная')

@section('content')
	<div class="container-fluid">
		@include('layouts.partials.header')
	</div>

	<div class="container gallery-block">
		<div class="row">
			<div class="col-md-12 text-center">
				<p class="gallery-block__title">Наша галерея</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6 col-sm-6 col-12 gallery-block__item">
					<div class="gallery-block__item-wrap">
						<img class="gallery-block__image img-fluid" src="img/gallery1.jpg" alt="">
						<p class="gallery-block__text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>
						<div class="gallery-block__action-wrap">
						 	<a href="gallery-detail" class="gallery-block__action">Подробнее
							 <img src="img/icons/left-arrow.svg" alt="">
							</a>
						</div>					
					</div>
			</div>

			<div class="col-md-6 col-sm-6 col-12 gallery-block__item">
					<div class="gallery-block__item-wrap">
						<img class="gallery-block__image img-fluid" src="img/gallery2.jpg" alt="">
						<p class="gallery-block__text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>
						<div class="gallery-block__action-wrap">
						 	<a href="gallery-detail" class="gallery-block__action">Подробнее
							 <img src="img/icons/left-arrow.svg" alt="">
							</a>
						</div>					
					</div>
			</div>			

			<div class="col-md-6 col-sm-6 col-12 gallery-block__item">
					<div class="gallery-block__item-wrap">
						<img class="gallery-block__image img-fluid" src="img/gallery3.jpg" alt="">
						<p class="gallery-block__text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>
						<div class="gallery-block__action-wrap">
						 	<a href="gallery-detail" class="gallery-block__action">Подробнее
							 <img src="img/icons/left-arrow.svg" alt="">
							</a>
						</div>					
					</div>
			</div>			

			<div class="col-md-6 col-sm-6 col-12 gallery-block__item">
					<div class="gallery-block__item-wrap">
						<img class="gallery-block__image img-fluid" src="img/gallery4.jpg" alt="">
						<p class="gallery-block__text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>
						<div class="gallery-block__action-wrap">
						 	<a href="gallery-detail" class="gallery-block__action">Подробнее
							 <img src="img/icons/left-arrow.svg" alt="">
							</a>
						</div>					
					</div>
			</div>			

			<div class="col-md-6 col-sm-6 col-12 gallery-block__item">
					<div class="gallery-block__item-wrap">
						<img class="gallery-block__image img-fluid" src="img/gallery5.jpg" alt="">
						<p class="gallery-block__text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>
						<div class="gallery-block__action-wrap">
						 	<a href="gallery-detail" class="gallery-block__action">Подробнее
							 <img src="img/icons/left-arrow.svg" alt="">
							</a>
						</div>					
					</div>
			</div>			

			<div class="col-md-6 col-sm-6 col-12 gallery-block__item">
					<div class="gallery-block__item-wrap">
						<img class="gallery-block__image img-fluid" src="img/gallery6.jpg" alt="">
						<p class="gallery-block__text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>
						<div class="gallery-block__action-wrap">
						 	<a href="gallery-detail" class="gallery-block__action">Подробнее
							 <img src="img/icons/left-arrow.svg" alt="">
							</a>
						</div>					
					</div>
			</div>			

			<div class="col-md-6 col-sm-6 col-12 gallery-block__item">
					<div class="gallery-block__item-wrap">
						<img class="gallery-block__image img-fluid" src="img/gallery7.jpg" alt="">
						<p class="gallery-block__text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>
						<div class="gallery-block__action-wrap">
						 	<a href="gallery-detail" class="gallery-block__action">Подробнее
							 <img src="img/icons/left-arrow.svg" alt="">
							</a>
						</div>					
					</div>
			</div>			

			<div class="col-md-6 col-sm-6 col-12 gallery-block__item">
					<div class="gallery-block__item-wrap">
						<img class="gallery-block__image img-fluid" src="img/gallery8.jpg" alt="">
						<p class="gallery-block__text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>
						<div class="gallery-block__action-wrap">
							<a href="gallery-detail" class="gallery-block__action">Подробнее
							 <img src="img/icons/left-arrow.svg" alt="">
							</a>
						</div>					
					</div>
			</div>			

					

		</div>
	</div>

	


	<div class="container-fluid footer-container">
		@include('layouts.partials.footer')
	</div>
@endsection