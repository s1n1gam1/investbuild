@extends('layouts.app')

@section('title', 'Новости')

@section('content')
	<div class="container-fluid">
		@include('layouts.partials.header')
	</div>
	
	<div class="container news-block mt-5">
		<div class="row">
			<div class="col-md-12 text-center">
				<p class="news-block__title">Наша галерея</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-10 offset-md-1 news-block__item-wrap">
				<img class="news-block__item-img" src="/img/gallery8.jpg" alt="">
				<div class="news-block__right">
					<p class="news-block__item-title">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>
					<p class="news-block__item-annotation">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
					<div class="news-block__action-wrap">
					 	<a href="news-detail" class="news-block__action">Подробнее
						 <img src="img/icons/left-arrow.svg" alt="">
						</a>
					</div>						
				</div>				
			</div>
			<div class="col-md-10 offset-md-1 news-block__item-wrap">
				<img class="news-block__item-img" src="/img/gallery8.jpg" alt="">
				<div class="news-block__right">
					<p class="news-block__item-title">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>
					<p class="news-block__item-annotation">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
					<div class="news-block__action-wrap">
					 	<a href="news-detail" class="news-block__action">Подробнее
						 <img src="img/icons/left-arrow.svg" alt="">
						</a>
					</div>						
				</div>				
			</div>			
			<div class="col-md-10 offset-md-1 news-block__item-wrap">
				<img class="news-block__item-img" src="/img/gallery8.jpg" alt="">
				<div class="news-block__right">
					<p class="news-block__item-title">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>
					<p class="news-block__item-annotation">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
					<div class="news-block__action-wrap">
					 	<a href="news-detail" class="news-block__action">Подробнее
						 <img src="img/icons/left-arrow.svg" alt="">
						</a>
					</div>						
				</div>				
			</div>			
			<div class="col-md-10 offset-md-1 news-block__item-wrap">
				<img class="news-block__item-img" src="/img/gallery8.jpg" alt="">
				<div class="news-block__right">
					<p class="news-block__item-title">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>
					<p class="news-block__item-annotation">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
					<div class="news-block__action-wrap">
					 	<a href="news-detail" class="news-block__action">Подробнее
						 <img src="img/icons/left-arrow.svg" alt="">
						</a>
					</div>						
				</div>				
			</div>	

			<div class="col-md-12">
				<div class="pagination-block">
					<a href="#" class="pagination-block__item prev">Пред.</a>
					<a href="#" class="pagination-block__item active">1</a>
					<a href="#" class="pagination-block__item">2</a>
					<a href="#" class="pagination-block__item">3</a>
					<a href="#" class="pagination-block__item next">След.</a>
				</div>					
			</div>	
		</div>
	</div>


	<div class="container-fluid footer-container">
		@include('layouts.partials.footer')
	</div>
@endsection