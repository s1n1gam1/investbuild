@extends('layouts.app')

@section('title', trans('app.mats.title'))

@section('content')
	<div class="container-fluid">
		@include('layouts.partials.header')
	</div>
	
	<div class="container build-block mt-5">
		<div class="row">
			<div class="col-md-12 text-center">
				<p class="build-block__title">{{trans('app.mats.title_1')}}</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-10 offset-md-1 build-block__item-wrap">
				<img class="build-block__item-img" src="/img/build/build1.jpg" alt="">
				<div class="build-block__right">
					<p class="build-block__item-title">{{trans('app.mats.desc_0')}}</p>
					<p class="build-block__item-annotation">{{trans('app.mats.desc_1')}}:</p>
					<p class="build-block__item-annotation">
						<a href="tel:871724575608">8 (7172) 45 75 608</a>
					</p>
					<p class="build-block__item-annotation">
						<a href="tel:87016406839">8 (701) 640 68 39</a>
					</p>
					<div class="build-block__action-wrap">
					 	<a  class="cbtn cbtn-light  cbtn-order-open">
					 		@include('svg.call')
					 		{{trans('app.mats.call')}}
					 	</a>
						<a href="/build_materials/price_concrete.pdf" class="cbtn cbtn-dark">
					 		@include('svg.pdf')
							{{trans('app.mats.download')}}
					 	</a>					 	
					</div>							
				</div>				
			</div>
			<div class="col-md-10 offset-md-1 build-block__item-wrap">
				<img class="build-block__item-img" src="/img/build/build2.jpg" alt="">
				<div class="build-block__right">
					<p class="build-block__item-title">{{trans('app.mats.desc_2')}}</p>
					<p class="build-block__item-annotation">{{trans('app.mats.desc_1')}}:</p>
					<p class="build-block__item-annotation">
						<a href="tel:87172940950">8 (7172) 940 950</a>
					</p>
					<p class="build-block__item-annotation">
						<a href="tel:87172940840">8 (7172) 940 840</a>
					</p>
					<div class="build-block__action-wrap">
					 	<a  class="cbtn cbtn-light  cbtn-order-open">
					 		@include('svg.call')
							{{trans('app.mats.call')}}
					 	</a>
						<a href="/build_materials/price_metalconstruct.pdf" class="cbtn cbtn-dark">
					 		@include('svg.pdf')
							{{trans('app.mats.download')}}
					 	</a>					 	
					</div>						
				</div>				
			</div>			
			<div class="col-md-10 offset-md-1 build-block__item-wrap">
				<img class="build-block__item-img" src="/img/build/build3.jpg" alt="">
				<div class="build-block__right">
					<p class="build-block__item-title">{{trans('app.mats.desc_3')}}</p>
					<p class="build-block__item-annotation">{{trans('app.mats.desc_1')}}:</p>
					<p class="build-block__item-annotation">
						<a href="tel:87172602200">8 (7172) 602 200</a>
					</p>
					<div class="build-block__action-wrap">
					 	<a  class="cbtn cbtn-light  cbtn-order-open">
					 		@include('svg.call')
							{{trans('app.mats.call')}}
					 	</a>
						<a href="" class="cbtn cbtn-dark">
					 		@include('svg.pdf')
							{{trans('app.mats.download')}}
					 	</a>					 	
					</div>							
				</div>				
			</div>			
		</div>
	</div>


	<div class="container-fluid footer-container">
		@include('layouts.partials.footer')
	</div>
@endsection