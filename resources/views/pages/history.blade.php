<div class="container-fluid history-block-wrap">
	<div class="history-block">
		<div class="history-block__content">

			<div class="history-block__section section_1">
				<p class="history-block__title">{{trans('app.history.title')}}</p>
			</div>

			<div class="history-block__section section_2">
				
				<div class="history-block__date-wrap">
					<p class="history-block__date-text">{{trans('app.history.since')}}</p>
					<p class="history-block__date">2003</p>
				</div>

				<img class="history-block__image" src="/img/history/history1.jpg" alt="">
			</div>

			<div class="history-block__section section_3">

				<p class="history-block__back-date">2004</p>

				<div class="history-block__text-wrap">
					<p class="history-block__text">{{trans('app.history.desc_0')}}</p>
					<p class="history-block__text">{{trans('app.history.desc_1')}}</p>
				</div>

				<img class="history-block__image_2" src="/img/history/history2.jpg" alt="">
			</div>

			<div class="history-block__section section_4">
				<p class="history-block__back-date sec_4">2005</p>
				
				<img class="history-block__image_2" src="/img/history/history3.jpg" alt="">

				<div class="history-block__text-wrap">
					<p class="history-block__text">{{trans('app.history.desc_2')}}</p>
					<p class="history-block__text">{{trans('app.history.desc_3')}}</p>
				</div>

			</div>	

			<div class="history-block__section section_5">
				

				<div class="history-block__text-wrap">
					<p class="history-block__text">{{trans('app.history.desc_4')}}</p>
				</div>

				<img class="history-block__image_2" src="/img/history/history4.jpg" alt="">

			</div>	


			<div class="history-block__section section_6">
				<p class="history-block__back-date sec_5">2010</p>

				<img class="history-block__image_3" src="/img/history/history5.jpg" alt="">
				
				<div class="history-block__text-wrap m">
					<p class="history-block__text">{{trans('app.history.desc_5')}}</p>
				</div>

			</div>	

			<div class="history-block__section section_7">
				<p class="history-block__back-date sec_6">2014</p>
				
				<img class="history-block__image_2" src="/img/history/history6.jpg" alt="">

				<div class="history-block__text-wrap">
					<p class="history-block__text">{{trans('app.history.desc_6')}}</p>
				</div>


			</div>	

			<div class="history-block__section section_8">

				<p class="history-block__back-date sec_7">2017</p>

				
				<div class="history-block__text-wrap m">
					<p class="history-block__text">{{trans('app.history.desc_7')}}</p>
				</div>

				<div class="history-block__image-wrapper">
					<img class="history-block__image_4" src="/img/history/history71.jpg" alt="">
					<img class="history-block__image_5" src="/img/history/history72.jpg" alt="">
				</div>

			</div>	


			<div class="history-block__section section_9">
				<p class="history-block__back-date sec_8">2018</p>

				
				<div class="history-block__text-wrap m">
					<p class="history-block__text">{{trans('app.history.desc_8')}}</p>
				</div>

				<div class="history-block__image-wrapper">
					<img class="history-block__image_6" src="/img/history/history8.jpg" alt="">
				</div>

			</div>	


			<div class="history-block__section">

				
				<div class="history-block__text-wrap">
					<p class="history-block__text">{{trans('app.history.desc_9')}}</p>
				</div>

			</div>																	

		</div>

	</div>

	<div class="scroll-block">
		<input type="range" id="range_1" class="range-component" min="0" max="420" step="1" value="0">
	</div>
</div>