@extends('layouts.app')

@section('title', 'Главная')

@section('content')
	<div class="container-fluid">
		@include('layouts.partials.header')
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-12 gallery-detail__left">
				<a href="gallery" class="gallery-detail__back">
					<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="0 0 21.825 21.825" style="enable-background:new 0 0 21.825 21.825;" xml:space="preserve">
					<path style="fill:#ff6600;" d="M16.791,13.254c0.444-0.444,1.143-0.444,1.587,0c0.429,0.444,0.429,1.143,0,1.587l-6.65,6.651
						c-0.206,0.206-0.492,0.333-0.809,0.333c-0.317,0-0.603-0.127-0.81-0.333l-6.65-6.651c-0.444-0.444-0.444-1.143,0-1.587
						s1.143-0.444,1.587,0l4.746,4.762V1.111C9.791,0.492,10.299,0,10.918,0c0.619,0,1.111,0.492,1.111,1.111v16.904L16.791,13.254z"/><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
					</svg>
					<p>Вернутся назад</p>
				</a>
			</div>

			<div class="col-md-12">
				<p class="invest__text">
					Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry's standard
				</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<hr class="hr">
			</div>
			<div class="col-md-3 col-sm-12 col-12 gallery-images-block__banner">
				<img class="img-fluid" src="/img/gallery1.jpg" alt="">
			</div>
		</div>

		<div class="row gallery-images-block">
			<div class="offset-md-1 col-md-10 ">
				<div class="gallery-images-block__wrap">

					<a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="This is my title" data-caption="Some lovely red flowers" data-image="/img/gallery-in1.jpg" data-target="#image-gallery">
						<img src="/img/gallery-in1.jpg" alt="" class="gallery-images-block__images img-fluid">	
					</a>

					<a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="This is my title" data-caption="Some lovely red flowers" data-image="/img/gallery-in2.jpg" data-target="#image-gallery">
						<img src="/img/gallery-in2.jpg" alt="" class="gallery-images-block__images img-fluid">	
					</a>

					<a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="This is my title" data-caption="Some lovely red flowers" data-image="/img/gallery-in3.jpg" data-target="#image-gallery">
						<img src="/img/gallery-in3.jpg" alt="" class="gallery-images-block__images img-fluid">	
					</a>

					<a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="This is my title" data-caption="Some lovely red flowers" data-image="/img/gallery-in1.jpg" data-target="#image-gallery">
						<img src="/img/gallery-in1.jpg" alt="" class="gallery-images-block__images img-fluid">	
					</a>

					<a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="This is my title" data-caption="Some lovely red flowers" data-image="/img/gallery-in2.jpg" data-target="#image-gallery">
						<img src="/img/gallery-in2.jpg" alt="" class="gallery-images-block__images img-fluid">	
					</a>

					<a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="This is my title" data-caption="Some lovely red flowers" data-image="/img/gallery-in3.jpg" data-target="#image-gallery">
						<img src="/img/gallery-in3.jpg" alt="" class="gallery-images-block__images img-fluid">	
					</a>

					<a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="This is my title" data-caption="Some lovely red flowers" data-image="/img/gallery-in1.jpg" data-target="#image-gallery">
						<img src="/img/gallery-in1.jpg" alt="" class="gallery-images-block__images img-fluid">	
					</a>

					<a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="This is my title" data-caption="Some lovely red flowers" data-image="/img/gallery-in2.jpg" data-target="#image-gallery">
						<img src="/img/gallery-in2.jpg" alt="" class="gallery-images-block__images img-fluid">	
					</a>

					<a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="This is my title" data-caption="Some lovely red flowers" data-image="/img/gallery-in3.jpg" data-target="#image-gallery">
						<img src="/img/gallery-in3.jpg" alt="" class="gallery-images-block__images img-fluid">					
					</a>

				</div>
			</div>
		</div>
	</div>

<div class="container">
		<div class="row">
			<div class="gallery-modal modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

				<div class="modal-dialog">
					<div class="modal-content">

						<div class="modal-header">
							
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
							
						</div>

						<div class="modal-body">
							<img id="image-gallery-image" class="img-responsive" src="">
						</div>

						<div class="modal-footer">
							<div class="col-md-2 start">
								<img id="show-previous-image" src="/img/icons/left-arrow.svg" alt="">
							</div>

							<div class="col-md-2 next">
								<img id="show-next-image" src="/img/icons/left-arrow.svg" alt="">
							</div>
						</div>

					</div>
				</div>

			</div>
		</div>
	</div>	

		<div class="row">
			<div class="col-md-12">
				<hr class="hr">
			</div>
		</div>	

	<div class="container-fluid footer-container">
		@include('layouts.partials.footer')
	</div>
@endsection	