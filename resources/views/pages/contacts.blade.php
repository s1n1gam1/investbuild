@extends('layouts.app')

@section('title', trans('app.menu.contacts'))

@section('content')
	<div class="container-fluid">
		@include('layouts.partials.header')
	</div>

	<div class="container contacts-block">
		<div class="row contacts-block__wrap">
			<div class="col-md-4 col-sm-12 col-12 offset-md-1 contacts-block__info">
				<p class="contacts-block__title">{{trans('app.contacts.title')}}</p>
				<p class="contacts-block__text">{{trans('app.contacts.address')}}</p>
				<p class="contacts-block__text">{{trans('app.contacts.phones')}}: <br>
					<a class="contacts-block__link" href="tel:87172940940"> 940-940</a>,
					<a class="contacts-block__link" href="tel:87172940941, ">940-941, </a>
					<a class="contacts-block__link" href="tel:87172940942, ">940-942, </a>
					<a class="contacts-block__link" href="tel:+77782142696">+7 778 214 26 96</a>
				</p>
				<p class="contacts-block__text">e-mail: 
					<a class="contacts-block__link" href="mailt:info@stroyinvest-sk.kz">info@stroyinvest-sk.kz</a>
				</p>
			</div>
			<div class="col-md-6 col-sm-12 col-12 order-md-last order-first">
				<iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A7d8c224b6ebf536c08c3680c116ab3ae41a9ff92575aa9f178054c367276f851&amp;source=constructor" width="100%" height="400" frameborder="0"></iframe>
			</div>
		</div>
	</div>

   <div class="container-fluid footer-container">
		@include('layouts.partials.footer')
	</div>	
@endsection
