@extends('layouts.app')

@section('title', trans('app.main_page_title'))

@section('js-head')
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&apikey=AFf3x1sBAAAAE1-0TgMAT256IBLPxF-BNSAeCjGi9AiUZtwAAAAAAAAAAADMxPCADL5Ne5DE7YilnCbAb1ccYA==&mode=release&&load=Map,Placemark,GeoObjectCollection"
            type="text/javascript"></script>
@endsection


@section('content')
    <div id="fullpage">
        <div class="section"  data-anchor="main-slider">
            <div class="container-fluid main-page">
                @include('layouts.partials.header')

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-12 main-about">
                        <div class="main-about__slider-wrapper">

                            <div class="main-about__slider-wrapper-left"></div>
                            <div class="main-about__slider-wrapper-right">
                                <img id="index_slider_image_1" src="/img/main1.jpg"
                                     class="main-about__slider-wrapper__image"
                                     alt="">
                                @foreach($sliders as $key => $slider)
                                    <img id="index_slider_image_{{++$key + 1}}"
                                         src="{{url($slider->image->getFullImage())}}"
                                         class="main-about__slider-wrapper__image {{--resetStyles--}}"
                                         alt="">
                                @endforeach

                            </div>
                        </div>


                        <div class="main-about__left">

                            <div class="main-about__text-wrapper hidden_block" id="index_slider_text_1">
                                <h1>@lang('app.site.title')</h1>
                                <h4>@lang('app.site.description')</h4>
                                <a href="{{route('about')}}" class="accent-btn main-about__button">@lang('app.about.title')</a>
                            </div>


                            @foreach($sliders as $key => $slider)
                                <div data-count='{{$loop->count + 1}}' class="main-about__text-wrapper hidden_block"
                                     id="index_slider_text_{{++$key + 1}}">
                                     <h1>{{$slider->title}}</h1>
                                    <h4>{{str_limit($slider->short_desc, $limit = 75, $end = '...') }}</h4>
                                    <a href="{{$slider->btn_url}}"
                                       class="accent-btn main-about__button">{{$slider->btn_label}}</a>
                                </div>
                            @endforeach

                            <div class="main-about__controllers">
                                <div data-count="{{$sliders->count() + 1}}" id="index_slider_prev"
                                     class="main-about__controller-wrap">
                                    @component('layouts.blocks.arrow',['type'=>'left'])
                                    @endcomponent
                                </div>
                                <div data-count="{{$sliders->count() + 1}}" id="index_slider_next"
                                     class="main-about__controller-wrap">
                                    @component('layouts.blocks.arrow',['type'=>'right'])
                                    @endcomponent
                                </div>
                            </div>


                            <div class="main-about__dots-wrap">
                                <div id="index_slider_dot_1" class="main-about__dots"></div>
                                @foreach($sliders as $key => $slider)
                                    <div id="index_slider_dot_{{++$key +1}}" class="main-about__dots"></div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section"  data-anchor="main-info">
            <div class="container-fluid main-page">
                <div class="row main-info">
                    <div class="col-md-12 col-sm-12 col-12 main-info__wrap">

                        <div class="main-info__left">
                            <div class="main-info__image-wrap">
                                <p class="main-info__year">15</p>
                                <p class="main-info__year-text">@lang('app.main_slide_1_title_1')</p>
                            </div>
                        </div>

                        <div class="main-info__right">
                            <p class="main-info__title">@lang('app.main_slide_1_title_2')</p>
                            <p class="main-info__text">@lang('app.main_slide_1_description')</p>
                            <a href="{{route('about')}}" class="accent-btn main-about__button">@lang('app.about.title')</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="section"  data-anchor="main-list">
            <div class="container-fluid main-page">
                <div class="row main-advantage">
                    <div class="col-md-6 col-sm-6 col-12 main-advantage__left">
                        <p class="main-page__title main-advantage__title">@lang('app.advantages.title'):</p>
                    </div>
                    <div class="col-md-6 col-sm-6 col-12 main-advantage__right">

                        <div class="main-advantage__item">
                            <img src="img/icons/plastic.svg" alt="" class="main-advantage__icon">
                            <div class="main-advantage__text-wrap">
                                <p class="main-advantage__item-title">@lang('app.advantages.list.0.title')</p>
                                <p class="main-advantage__item-text">@lang('app.advantages.list.0.description')</p>
                            </div>
                        </div>

                        <div class="main-advantage__item">
                            <img src="img/icons/cement-truck.svg" alt="" class="main-advantage__icon">
                            <div class="main-advantage__text-wrap">
                                <p class="main-advantage__item-title">@lang('app.advantages.list.1.title')</p>
                                <p class="main-advantage__item-text">@lang('app.advantages.list.1.description')</p>
                            </div>
                        </div>

                        <div class="main-advantage__item">
                            <img src="img/icons/crane.svg" alt="" class="main-advantage__icon">
                            <div class="main-advantage__text-wrap">
                                <p class="main-advantage__item-title">@lang('app.advantages.list.2.title')</p>
                                <p class="main-advantage__item-text">@lang('app.advantages.list.2.description')</p>
                            </div>
                        </div>

                        <div class="main-advantage__item">
                            <img src="img/icons/worker.svg" alt="" class="main-advantage__icon">
                            <div class="main-advantage__text-wrap">
                                <p class="main-advantage__item-title">@lang('app.advantages.list.3.title')</p>
                                <p class="main-advantage__item-text">@lang('app.advantages.list.3.description')</p>
                            </div>
                        </div>

                        <div class="main-advantage__item">
                            <img src="img/icons/park.png" alt="" class="main-advantage__icon">
                            <div class="main-advantage__text-wrap">
                                <p class="main-advantage__item-title">@lang('app.advantages.list.4.title')</p>
                                <p class="main-advantage__item-text">@lang('app.advantages.list.4.description')</p>
                            </div>
                        </div>

                        <div class="main-advantage__item">
                            <img src="img/icons/smart-house.png" alt="" class="main-advantage__icon">
                            <div class="main-advantage__text-wrap">
                                <p class="main-advantage__item-title">@lang('app.advantages.list.5.title')</p>
                                <p class="main-advantage__item-text">@lang('app.advantages.list.5.description')</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <div class="section" id="objects-slider"  data-anchor="main-second-slider">
            <div class="container-fluid main-page">
                <div class="row main-objects">
                    <div class="col-md-12 col-sm-12 col-12 main-objects__wrap">
                        
                        <div data-count="3" class="main-objects__left">

							<div id="index_objects_text_1" class="main-objects__text-wrap">
								<p class="main-page__title main-objects__title">@lang('app.objects.title'):</p>
								<p class="main-objects__name">@lang('app.objects.list.0.title') </p>
								<p class="main-objects__text">@lang('app.objects.list.0.description')</p>
								<a href="{{route('projects.index')}}" class="custom-btn main-objects__btn">@lang('app.objects.button')</a>
							</div>							

							<div id="index_objects_text_2" class="main-objects__text-wrap">
								<p class="main-page__title main-objects__title">@lang('app.objects.title'):</p>
								<p class="main-objects__name">@lang('app.objects.list.1.title')</p>
								<p class="main-objects__text">@lang('app.objects.list.1.description')</p>
								<a href="{{route('projects.index')}}" class="custom-btn main-objects__btn">@lang('app.objects.button')</a>
							</div>	

                            

							<div id="index_objects_text_3" class="main-objects__text-wrap">
								<p class="main-page__title main-objects__title">@lang('app.objects.title'):</p>
								<p class="main-objects__name">@lang('app.objects.list.2.title')</p>
								<p class="main-objects__text">@lang('app.objects.list.2.description')</p>
								<a href="{{route('projects.index')}}" class="custom-btn main-objects__btn">@lang('app.objects.button')</a>
							</div>

                            <div class="main-objects__controllers">
                                <div id="index_object_prev" class="main-objects__controller-wrap">
                                    @component('layouts.blocks.arrow',['type'=>'left'])
                                    @endcomponent
                                </div>
                                <div id="index_object_next" class="main-objects__controller-wrap ">
                                    @component('layouts.blocks.arrow',['type'=>'right'])
                                    @endcomponent
                                </div>
                            </div>                            							
						</div>

                        <div class="main-objects__right order-sm-last order-md-last order-lg-last order-xl-last order-first">
                            <img id="index_object_image_1" src="/img/slide-objects/slid-object5.jpg"
                                 class="main-objects__image" alt="">
                            <img id="index_object_image_2" src="/img/slide-objects/slid-object7.jpg"
                                 class="main-objects__image" alt="">
                            <img id="index_object_image_3" src="/img/slide-objects/slid-object6.jpg"
                                 class="main-objects__image" alt="">
                        </div>


                </div>
            </div>
        </div>
    </div>

        <div class="section"  data-anchor="main-map">
            <div class="container-fluid main-page bottom">
                <div class="row main-map">
                    <div class="col-md-10 offset-md-1 col-sm-10 offset-sm-1 col-12  main-map__wrap">
                        <div class="main-map__left">
                            <p class="main-page__title main-map__title">@lang('app.astana')</p>
                            <p id="setObject1" class="main-map__object">@lang('app.avicenna')</p>
                            <p id="setObject2" class="main-map__object">@lang('app.avicenna_2')</p>
                            <p id="setObject3" class="main-map__object">@lang('app.abay_otyrar')</p>
                            <p id="setObject4" class="main-map__object">@lang('app.abay_2')</p>
                            <p id="setObject5" class="main-map__object">@lang('app.avicenna_elite')</p>
                            <p id="setObject6" class="main-map__object">@lang('app.imanova')</p>
                        </div>
                        <div class="main-map__right  order-md-last order-lg-last order-xl-last order-sm-first order-first">
                            <div id="map"></div>
                        </div>
                    </div>
                </div>

            </div>
            @include('layouts.partials.footer',['foolpage'=>true])
        </div>


    </div>




@endsection
