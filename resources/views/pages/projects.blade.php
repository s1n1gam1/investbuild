@extends('layouts.app')
@section('title', 'Наши объекты')
@section('content')
<div class="container-fluid">
	@include('layouts.partials.header')
</div>
<div class="container mar-bot">
	<div class="row">
		<div class="col-md-12 text-center">
			<p class="main-block__title">Наши объекты</p>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-12">
				<ul class="nav nav-pills mb-3 custom-navs" id="pills-tab" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Все объекты</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">
							<img src="/img/icons/build.png" alt="">
							Жилые комплексы
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">
							<img src="/img/icons/factory.png" alt="">
							Промышленный сектор
						</a>
					</li>
				</ul>
				<div class="tab-content" id="pills-tabContent">
					<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
						<div class="container">
							<div class="row">
								<div class="col-md-6 col-sm-12 col-12">
									<div class="stat-block">
										<p class="stat-block__number">47</p>
										<p class="stat-block__text">жилых и промышленных объектов построено</p>
									</div>

									<div class="projects-info">
										<img class="projects-info__image" src="/img/projects/projects1.jpg" alt="">
										<div class="projects-info__text-wrap">
											<p class="projects-info__title">Жилой комплекс «Абай»</p>
											<p class="projects-info__date">Сдан: 2004 год</p>
											<div class="projects-info__action-wrap">
											 	<a href="project-inner" class="projects-info__action">Подробнее
												 <img src="img/icons/left-arrow.svg" alt="">
												</a>
											</div>												
										</div>
									</div>
									<div class="projects-info">
										<img class="projects-info__image" src="/img/projects/projects2.jpg" alt="">
										<div class="projects-info__text-wrap">
											<p class="projects-info__title">Жилой комплекс «Абай 2»</p>
											<p class="projects-info__date">Сдан: 2004 год</p>
											<div class="projects-info__action-wrap">
											 	<a href="project-inner" class="projects-info__action">Подробнее
												 <img src="img/icons/left-arrow.svg" alt="">
												</a>
											</div>												
										</div>
									</div>									
									<div class="projects-info">
										<img class="projects-info__image" src="/img/projects/projects3.jpg" alt="">
										<div class="projects-info__text-wrap">
											<p class="projects-info__title">Жилой комплекс «Авиценна»</p>
											<p class="projects-info__date">Сдан: 2004 год</p>
											<div class="projects-info__action-wrap">
											 	<a href="project-inner" class="projects-info__action">Подробнее
												 <img src="img/icons/left-arrow.svg" alt="">
												</a>
											</div>												
										</div>
									</div>											
								</div>
								<div class="col-md-6 col-sm-12 col-12">
									<div class="projects-info">
										<img class="projects-info__image" src="/img/projects/projects4.jpg" alt="">
										<div class="projects-info__text-wrap">
											<p class="projects-info__title">Жилой комплекс «Авиценна 2»</p>
											<p class="projects-info__date">Сдан: 2004 год</p>
											<div class="projects-info__action-wrap">
											 	<a href="project-inner" class="projects-info__action">Подробнее
												 <img src="img/icons/left-arrow.svg" alt="">
												</a>
											</div>												
										</div>
									</div>										
									<div class="projects-info">
										<img class="projects-info__image" src="/img/projects/projects5.jpg" alt="">
										<div class="projects-info__text-wrap">
											<p class="projects-info__title">Жилой комплекс «Авиценна Элит»</p>
											<p class="projects-info__date">Сдан: 2004 год</p>
											<div class="projects-info__action-wrap">
											 	<a href="project-inner" class="projects-info__action">Подробнее
												 <img src="img/icons/left-arrow.svg" alt="">
												</a>
											</div>												
										</div>
									</div>										
								</div>
							</div>
						</div>
					</div>

					<div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
						<div class="container">
							<div class="row">
								<div class="col-md-6 col-sm-12 col-12">
									<div class="projects-info">
										<img class="projects-info__image" src="/img/projects/projects6.png" alt="">
										<div class="projects-info__text-wrap">
											<p class="projects-info__title">Бейнеуский зерновой терминал</p>
											<p class="projects-info__date">Сдан: 2004 год</p>
											<div class="projects-info__action-wrap">
											 	<a href="project-inner" class="projects-info__action">Подробнее
												 <img src="img/icons/left-arrow.svg" alt="">
												</a>
											</div>												
										</div>
									</div>										
								</div>
								<div class="col-md-6 col-sm-12 col-12">
																		
								</div>
							</div>
						</div>
					</div>

					<div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
						<div class="container">
							<div class="row">
								
							</div>
						</div>
					</div>

				</div>
			</div>

			


		</div>


		
	</div>

</div>
	<div class="container-fluid footer-container">
		@include('layouts.partials.footer')
	</div>
@endsection