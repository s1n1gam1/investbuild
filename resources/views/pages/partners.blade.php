@extends('layouts.app')

@section('title', trans('app.partners.title'))

@section('content')
	<div class="container-fluid">
		@include('layouts.partials.header')
	</div>

	<div class="container mar-bot mt-5">
		<div class="row">
			<div class="col-md-12 text-center">
				<p class="gallery-block__title">{{trans('app.partners.trust_us')}}</p>
			</div>
		</div>		
		<div class="row">
			<div class="col-md-12">
				<hr class="hr">
			</div>	
		</div>

		<div class="row partners-block">
			<div class="col-md-2 col-sm-12 partners-block__left">
				
				<div class="partners-block__slider  partners-carousel">

					<img class="partners-block__image item" 
					     src="/img/partners/partners1.png" 
					     data-image="/img/partners/partners1.png"
					     data-text="Non pariatur fugiat tempor dolor adipisicing ut in et mollit in ex nostrud in eu dolore minim pariatur."
					     id="partner-element-1"
					     data-link="http://www.link1.com"
					     alt="">

					<img class="partners-block__image item" 
					     src="/img/partners/partners_colored_2.png" 
					     data-image="/img/partners/partners2.png"
					     data-text="Sunt aute sed ullamco ad velit nulla irure reprehenderit in."
					     id="partner-element-2"
					     data-link="http://www.link2.com"
					     alt="">

					<img class="partners-block__image item" 
					     src="/img/partners/partners3.png" 
					     data-image="/img/partners/partners3.png"
					     data-text="Qui occaecat incididunt culpa eu culpa aute dolor nulla ex laborum officia incididunt et officia adipisicing magna do."
					     id="partner-element-3"
					     data-link="http://www.link3.com"
					     alt="">

				</div>
			</div>

			<div class="col-md-1 col-sm-12 partners-block__nav">

				<div class="line"></div>

				<div class="partners-block__nav-arrows">

					<a class="partners-block__nav-arrow-wrapper top">
						<img class="partners-block__nav-arrow prev" src="/img/icons/down-arrow.svg" alt="">
					</a>

					<a class="partners-block__nav-arrow-wrapper bottom">
						<img class="partners-block__nav-arrow next" src="/img/icons/down-arrow.svg" alt="">
					</a>

				</div>
			</div>	

			<div class="col-md-9 col-sm-12 partners-block__right">
				<div class="partners-block__detailed-wrap">

					<img class="partners-block__image main" src="/img/partners/partners_colored_2.png" alt="">

					{!!  trans('app.partners.desc')!!}

					<div class="partners-block__action-wrap">
						 	<a target="_blank" href="http://energy-city.kz/" class="partners-block__action partners-block__link">{{trans('app.detail')}}
							 <img src="/img/icons/left-arrow.svg" alt="">
							</a>
						</div>						
				</div>
			</div>
		</div>

		

		<div class="row">
			<div class="col-md-12">
				<hr class="hr">
			</div>						
		</div>
	</div>


	<div class="container-fluid footer-container">
		@include('layouts.partials.footer')
	</div>
@endsection		