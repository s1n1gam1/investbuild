@extends('layouts.app')

@section('title', trans('app.about.title'))

@section('content')
<div id="fullpage">
	<div class="section" data-anchor="about-slider">
		<div class="container-fluid main-page">
			@include('layouts.partials.header')

			<div class="row">
				<div class="col-md-12 about-block">
					<div class="about-block__left">
						<h1 class="about-block__title">@lang('app.about.title')</h1>
						<h4 class="about-block__text">@lang('app.about.short_desc')</h4>
						<a target="_blank" href="{{route('projects.index')}}" class="accent-btn about-block__button">@lang('app.about.btn_obj')</a>
					</div>
				</div>
			</div>
		</div>		
	</div>

	<div class="section about-page"  data-anchor="about-info">
		<div class="container-fluid about-page">
			<div class="row about-text">

				<div class="about-text__left  about">
					<img src="/img/about/about-bg-2.jpg" alt="">
				</div>

				<div class="about-text__right">
					<p class="about-text__title">{{trans('app.site.title')}}</p>
					<p class="about-text__text">{{trans('app.about.short_desc_1')}}</p>
					<p class="about-text__text">{{trans('app.about.short_desc_2')}}</p>
					<p class="about-text__text">{{trans('app.about.short_desc_3')}}</p>
				</div>

			</div>
		</div>
	</div>

	<div class="section about-page"  data-anchor="about-second-slider">
		<div class="container-fluid">

			<div class="row service-types-tabs">
				<div class="col-sm-12 col-12 col-md-10 offset-md-1 col-lg-10 offset-lf-1 col-xl-10 offset-xl-1 service-types-tabs__wrap">
					<p class="service-types-tabs__title">
						{{trans('app.direction.title')}}
					</p>

					<div class="service-types-tabs__wrapper" data-slide-id="1">
						<p class="service-types-tabs__type">{{trans('app.direction.list.0.title')}}</p>
						<div id="service-types-tabs-1" class="active-item selected"></div>
					</div>

					<div class="service-types-tabs__wrapper" data-slide-id="2">
						<p class="service-types-tabs__type">{{trans('app.direction.list.1.title')}}</p>
						<div id="service-types-tabs-2" class="active-item"></div>
					</div>

					<div class="service-types-tabs__wrapper" data-slide-id="3">
						<p class="service-types-tabs__type">{{trans('app.direction.list.2.title')}}</p>
						<div id="service-types-tabs-3" class="active-item"></div>
					</div>

					<div class="service-types-tabs__wrapper" data-slide-id="4">
						<p class="service-types-tabs__type">{{trans('app.direction.list.3.title')}}</p>
						<div id="service-types-tabs-4" class="active-item"></div>
					</div>					


					<img class="service-types-tabs__tabs" src="/img/about/tabs.png" alt="">
				</div>
				
			</div>	



			<div class="row">

				<div class="col-md-12 slider-about">
					<div class="slider-about__image">

						<img id="slider-about-image-1" class="slider-about__tab-image active" src="/img/about/about-bg-3.jpg" alt="">
						<img id="slider-about-image-2" class="slider-about__tab-image" src="/img/about/busines-center.jpg" alt="">
						<img id="slider-about-image-3" class="slider-about__tab-image" src="/img/about/industrial.jpg" alt="">
						<img id="slider-about-image-4" class="slider-about__tab-image" src="/img/build/build1.jpg" alt="">
					</div>

					<div class="slider-about__arrows">
						<img id="slider_about_left"  class="slider-about__arrows-img  slider-about__arrows-left" src="/img/about/left.png" alt="">
						<img id="slider_about_right" class="slider-about__arrows-img slider-about__arrows-right" src="/img/about/right.png" alt="">
					</div>

					<div data-count="4" class="slider-about__text-block">
						
						<div id="slider_about_text_1" class="slider-about__text-block-wrap active">
							<h1 class="slider-about__title">{{trans('app.direction.list.0.title')}}</h1>
							<h4 class="slider-about__text">{{trans('app.direction.list.0.desc')}}</h4>
							<a target="_blank" href="projects" class="accent-btn slider-about__button">{{trans('app.projects')}}</a>
						</div>

						<div id="slider_about_text_2" class="slider-about__text-block-wrap">
							<h1 class="slider-about__title">{{trans('app.direction.list.1.title')}}</h1>
							<h4 class="slider-about__text">{{trans('app.direction.list.1.desc')}}</h4>
							<a target="_blank" href="projects" class="accent-btn slider-about__button">{{trans('app.projects')}}</a>
						</div>						

						<div id="slider_about_text_3" class="slider-about__text-block-wrap">
							<h1 class="slider-about__title">{{trans('app.direction.list.2.title')}}</h1>
							<h4 class="slider-about__text">{{trans('app.direction.list.2.desc')}}</h4>
							<a target="_blank" href="projects" class="accent-btn slider-about__button">{{trans('app.projects')}}</a>
						</div>						

						<div id="slider_about_text_4" class="slider-about__text-block-wrap">
							<h1 class="slider-about__title">{{trans('app.direction.list.3.title')}}</h1>

							<p class="slider-about__text build">
								{{trans('app.direction.list.3.desc_0')}} <br>
								<a href="tel:87016406839">8 (701) 640 68 39 </a>
							</p>

							<p class="slider-about__text build">
								{{trans('app.direction.list.3.desc_1')}} <br>
								<a href="tel:87172940950">8 (7172) 940 950 </a>,
								<a href="tel:87172940840">8 (7172) 940 840 </a>
							</p>

							<p class="slider-about__text build">
								{{trans('app.direction.list.3.desc_2')}} <br>
								<a href="tel:87172602200">8 (7172) 602 200 </a>
							</p>

							<a target="_blank" href="build-materials" class="accent-btn slider-about__button">{{trans('app.menu.building_mats')}}</a>
						</div>						

					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="history-block" class="section about-page"  data-anchor="about-history">
		@include('pages.history')
	</div>



	<div class="section about-page"  data-anchor="about-text">
		<div class="container-fluid main-page bottom">
			<div class="row">
				<div class="col-md-12 today-block">
					<div class="today-block__left">
						<img  class="today-block__image" src="/img/about/about-page-4.png" alt="">
						<img class="today-block__dots" src="/img/main2bk.png" alt="">
					</div>

					<div class="today-block__right">
						<img class="today-block__cav left" src="/img/about/cav-left.png" alt="">
						<img class="today-block__cav right" src="/img/about/cav-right.png" alt="">
						<p class="today-block__text">
							{{trans('app.about.quote')}}
						</p>
					</div>
				</div>
			</div>
		</div>
		@include('layouts.partials.footer',['foolpage'=>true])
	</div>



</div>
	

	
@endsection