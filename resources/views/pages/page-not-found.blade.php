@extends('layouts.app')

@section('title', 'Главная')

@section('content')
	<div class="container-fluid">
		@include('layouts.partials.header')
	</div>

	<div class="container-fluid error-block">
		<div class="row">
			<div class="col-md-12 error-block__wrap">
				<div class="error-block__left">
					<h2>Страница временно недоступна</h1>
					<h4>Данная страница находится в разработке</h2>					
				</div>
				<div class="error-block__right">
					
				</div>
			</div>
		</div>
	</div>


	<div class="container-fluid footer-container">
		@include('layouts.partials.footer')
	</div>	
@endsection