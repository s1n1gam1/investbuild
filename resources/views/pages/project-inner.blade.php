@extends('layouts.app')

@section('title', 'Наши объекты')
@section('wrap_class', 'project-inner-block-wrap')

@section('content')
	<div class="container-fluid">
		@include('layouts.partials.header')
	</div>

	<div class="container project-inner-block mar-bot">
		<div class="row">
			<div class="col-md-6 col-sm-12 col-12 project-inner-block__slider">
				<img src="/img/project/project-inner1.jpg" alt="" class="project-inner-block__image main">
				<img src="/img/project/project-inner2.jpg" alt="" class="project-inner-block__image">
				<img src="/img/project/project-inner3.jpg" alt="" class="project-inner-block__image">
				<img src="/img/project/project-inner4.jpg" alt="" class="project-inner-block__image">
			</div>
			<div class="col-md-6 col-sm-12 col-12 project-inner-block__text-wrap">
				<p class="project-inner-block__title">Жилой комплекс “Авиценна 2”</p>

				<div class="project-inner-block__addit">
					<p class="project-inner-block__city">город Астана</p>
					<p class="project-inner-block__date">сдан: 2017 год</p>
				</div>

				<div class="project-inner-block__grids">
					<div class="project-inner-block__secondary">
						<p class="project-inner-block__secondary-main">37 158 м²</p>
						<p class="project-inner-block__secondary-text">Общая площадь здания</p>
					</div>

					<div class="project-inner-block__secondary">
						<p class="project-inner-block__secondary-main">8 блоков + паркинг</p>
						<p class="project-inner-block__secondary-text">Составляющие</p>
					</div>	

					<div class="project-inner-block__secondary">
						<p class="project-inner-block__secondary-main">от 9 до 18 этажей</p>
						<p class="project-inner-block__secondary-text">Этажность</p>
					</div>					
				</div>

				


				<div class="project-inner-block__action-wrap">
				 	<a href="projects" class="project-inner-block__action">Другие объекты
					 <img src="img/icons/left-arrow.svg" alt="">
					</a>
				</div>					
			</div>
		</div>




		<div class="row about-project">
			<div class="col-md-12 col-sm-12 col-12">
				<p class="about-project__title">О проекте</p>
				<p class="about-project__text">
					ЖК «Абай» расположен в центральной  части  города, по  улице Габдуллина. Жилой комплекс бизнес класса сдан в эксплуатацию и состоит из двух корпусов, включающих восемь блоков переменной этажности (9-16-18), в которых размещены 264 квартиры и офисные помещения. Рядом с домом расположен четырехэтажный паркинг для автомобилей. Высокое качество строительства, удобные и продуманные планировки квартир, уютный обжитый район, транспортная доступность — это далеко не все преимущества современного жилого комплекса.Дизайнерское решение, предполагающее гармоничное сочетание восточной мягкости очертаний и европейской практичности делают жилой комплекс полностью соответствующим облику казахстанской столицы – перекрестку восточной и западной культур.
				</p>				
			</div>
		</div>
	</div>

	<div class="container-fluid footer-container">
		@include('layouts.partials.footer')
	</div>
@endsection	