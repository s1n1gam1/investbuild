<div class="row footer {{!empty($foolpage)?'fullpage':'normal'}}">
	<div class="col-md-12 footer__wrap">
		<img src="/img/logo.png" class="footer__logo" alt="">

		<div class="footer__block">
			<p class="footer__title">@lang('app.address'):</p>
			<p class="footer__text">@lang('app.footer.address')</p>
		</div>

		<div class="footer__block">
			<p class="footer__title">@lang('app.phone'):</p>
			<a class="footer__text" href="tel:77172940940">+7 7172 940-940</a>
		</div>

		<div class="footer__block">
			<p class="footer__title">E-mail:</p>
			<a class="footer__text" href="mailto:info@stroyinvest-sk.kz">info@stroyinvest-sk.com</a>
		</div>

		<div class="footer__block">
			<p class="footer__title">@lang('app.footer.social'):</p>
			<p class="footer__text">

				<a target="_blank" class="footer__social-link" href=https://www.youtube.com/channel/UC5uCA07XgsUohOfLaTuT4Ig/featured?disable_polymer=1>
					@component('layouts.svg-blocks.youtube')
					@endcomponent				
				</a>
				
				<a target="_blank" class="footer__social-link" href="https://www.instagram.com/stroyinvest.sk/">
					@component('layouts.svg-blocks.instagram')
					@endcomponent								
				</a>
				
				<a target="_blank" class="footer__social-link" href="https://www.facebook.com/%D0%A1%D1%82%D1%80%D0%BE%D0%B9%D0%B8%D0%BD%D0%B2%D0%B5%D1%81%D1%82-%D0%A1%D0%9A-1945602948849422/">
					@component('layouts.svg-blocks.facebook')
					@endcomponent								
				</a>
				
			</p>
		</div>		
	</div>

	
</div>