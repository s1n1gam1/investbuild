<div class="row header">
	<div class="offset-md-1 col-md-10  header__wrap">

		<div class="header__lang">

			<div id='open-menu' class="hamburger hamburger--collapse-r">
			    <div class="hamburger-box">
			      <div class="hamburger-inner"></div>
			    </div>
			  </div>

			<div class="header__lang-items">
				<a href="/ru" class="header__lang-item active">Ru</a>
				<a href="/kz" class="header__lang-item">Kaz</a>
				<a href="/en" class="header__lang-item">Eng</a>
			</div>
		</div>

		<div class="header__logo-wrap">
			<a href="/">
				<img class="header__logo img-responsive" src="/img/logo.png" alt="">
			</a>
		</div>
		
		<div id='open-order' class="header__order">
			<p class="header__order-number">+7 7172 940 940</p>
			<p class="header__order-button">{{trans('app.order_to_call')}}</p>
		</div>

	</div>
</div>
