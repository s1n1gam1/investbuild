<div id="order-build" class="container-fluid order">
	
	<div class="row order__wrap">
		<div class="col-md-10 offset-md-1 order__close-wrap">
			<div id='close-order-build' class="hamburger hamburger--collapse-r">
			    <div class="hamburger-box">
			      <div class="hamburger-inner"></div>
			    </div>
			  </div>
		</div>
	</div>

	<div class="row order__content">
		<div class="col-md-12 order__content-wrap">
			<p class="order__title">{{trans('app.order.title')}}</p>
			<form class="order__form" action="{{route('feedback')}}" method="post">
				@csrf
				<input name="name" required class="order__input" type="text" placeholder="{{trans('app.order.name')}}">
				<input name="phone" required class="order__input order__input-phone" type="text" placeholder="{{trans('app.order.phone')}}">
				<input name="comments" required class="order__input order__input" type="text" placeholder="{{trans('app.order.comment')}}">
				<button class="order__button" type="submit">{{trans('app.order.submit')}}</button>
			</form>
		</div>
	</div>
</div>