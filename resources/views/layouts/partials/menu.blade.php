<div class="container-fluid menu">


	<div class="row menu__items">
		<div class="col-md-10 offset-md-1 menu__items-wrap">
			<a href="/" class="menu__item active">{{trans('app.menu.main')}}</a>
			<a href="/about" class="menu__item">{{trans('app.menu.about')}}</a>
			<a href="/projects" class="menu__item">{{trans('app.menu.objects')}}</a>
			<a href="/build-materials" class="menu__item">{{trans('app.menu.building_mats')}}</a>
			<a href="/news" class="menu__item">{{trans('app.menu.news')}}</a>
			<a href="/partners" class="menu__item">{{trans('app.menu.partners')}}</a>
			<a href="/gallery" class="menu__item">{{trans('app.menu.gallery')}}</a>
			<a href="/contacts" class="menu__item">{{trans('app.menu.contacts')}}</a>
		</div>

		<div class="col-md-12 menu__lang">
			<a href="/ru" class="menu__lang-item active">Ru</a>
			<a href="/kz" class="menu__lang-item">Kaz</a>
			<a href="/en" class="menu__lang-item">Eng</a>
		</div>
	</div>
</div>