/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
document.getElementById("app").addEventListener("wheel", function() {
    $('.order__content').css('height', '0vh');
    $('#close-order').removeClass('is-active')
    setTimeout(function() {
        $('.order').css('z-index', '-9998');
    }, 400);
    $('#open-menu').removeClass('is-active')
    $('.menu__items').css('height', '0vh');
    setTimeout(function() {
        $('.menu').css('z-index', '-9998');
    }, 400);
});
//Additional scripts
jQuery(document).ready(function($) {

    let width = $(window).width();
    if(width<480){
        $('#range_1').attr('max', '650');
    }
    else if(width<360){
        $('#range_1').attr('max', '750');
    }

    $('.change-text').text('Jquery Works');
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    });
    //For carousel
    $('.carousel .vertical .item').each(function() {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));
        for (var i = 1; i < 2; i++) {
            next = next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }
            next.children(':first-child').clone().appendTo($(this));
        }
    });
    $('#fullpage').fullpage({ 
        //options here
        autoScrolling: true,
        scrollHorizontally: true
    });
    //methods
    // $.fn.fullpage.setAllowScrolling(false);
    //script for menu
    $("#open-menu").click(function(event) {
        if ($(this).hasClass('is-active')) {
            $(this).removeClass('is-active')
            $('.menu__items').css('height', '0vh');
            setTimeout(function() {
                $('.menu').css('z-index', '-9998');
            }, 400);
        } else {
            $(this).addClass('is-active')
            $('.menu').css('z-index', '9998');
            $('.menu__items').css('height', '80vh');
            $('.menu__items').css('box-shadow', '0px 0px 5px #333');
        }
    });

    $("#open-order").click(function(event) {
        window.scrollTo(0, 0);

        $('#order-main').css('z-index', '999999');
        $('#order-main .order__content').css('height', '80vh');
        $('#close-order-main').addClass('is-active')
    });

    $(".cbtn-order-open").click(function(event) {
        window.scrollTo(0, 0);

        $('#order-build').css('z-index', '999999');
        $('#order-build .order__content').css('height', '80vh');
        $('#close-order-build').addClass('is-active')
    });    

    $(".open-order-from").click(function(event) {
        $('.order').css('z-index', '999999');
        $('.order__content').css('height', '80vh');
        $('#close-order').addClass('is-active')
        window.scrollTo(0, 0);
    });

    $("#close-order-main").click(function(event) {
        $('#order-main .order__content').css('height', '0vh');
        $('#close-order-main').removeClass('is-active')
        setTimeout(function() {
            $('.order').css('z-index', '-9998');
        }, 400);
    });

    $("#close-order-build").click(function(event) {
        $('#order-build .order__content').css('height', '0vh');
        $('#close-order-build').removeClass('is-active')
        setTimeout(function() {
            $('.order').css('z-index', '-9998');
        }, 400);
    });    
});
$(document).ready(function() {
    //script for objects detail view
    loadObjectsImage();

    function loadObjectsImage() {
        $('.project-inner-block__image').click(function(event) {
            if (!$(this).hasClass('main')) {
                console.log($(this).data('image'))
                $('.project-inner-block__image.main').attr('src', $(this).data('image'))
            }
        });
    }
    //Script for gallery images
    loadGallery(true, 'a.thumbnail');

    function disableButtons(counter_max, counter_current) {
        $('#show-previous-image, #show-next-image').show();
        if (counter_max == counter_current) {
            $('#show-next-image').hide();
        } else if (counter_current == 1) {
            $('#show-previous-image').hide();
        }
    }

    function loadGallery(setIDs, setClickAttr) {
        var current_image,
            selector,
            counter = 0;
        $('#show-next-image, #show-previous-image').click(function() {
            if ($(this).attr('id') == 'show-previous-image') {
                current_image--;
            } else {
                current_image++;
            }
            selector = $('[data-image-id="' + current_image + '"]');
            updateGallery(selector);
        });

        function updateGallery(selector) {
            var $sel = selector;
            current_image = $sel.data('image-id');
            $('#image-gallery-caption').text($sel.data('caption'));
            $('#image-gallery-title').text($sel.data('title'));
            $('#image-gallery-image').attr('src', $sel.data('image'));
            disableButtons(counter, $sel.data('image-id'));
        }
        if (setIDs == true) {
            $('[data-image-id]').each(function() {
                counter++;
                $(this).attr('data-image-id', counter);
            });
        }
        $(setClickAttr).on('click', function() {
            updateGallery($(this));
        });
    }
    
    //This is script for history block on about page
    $(document).on('input', '#range_1', function() {
        $('.history-block__content').css('left', '-' + $(this).val() + '%');
    });
});


//script for phone mask
var phoneInput = document.querySelector('.order__input-phone')
phoneInput.addEventListener('keydown', function(event) {
    if (!(event.key == 'ArrowLeft' || event.key == 'ArrowRight' || event.key == 'Backspace' || event.key == 'Tab')) {
        event.preventDefault()
    }
    var mask = '+7 (111) 111-11-11'; // Задаем маску
    if (/[0-9\+\ \-\(\)]/.test(event.key)) {
        // Здесь начинаем сравнивать this.value и mask
        // к примеру опять же
        var currentString = this.value;
        var currentLength = currentString.length;
        if (/[0-9]/.test(event.key)) {
            if (mask[currentLength] == '1') {
                this.value = currentString + event.key;
            } else {
                for (var i = currentLength; i < mask.length; i++) {
                    if (mask[i] == '1') {
                        this.value = currentString + event.key;
                        break;
                    }
                    currentString += mask[i];
                }
            }
        }
    }
});