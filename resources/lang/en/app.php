<?php
return [
    'order_to_call' => 'Call back',
    'main_page_title' => 'Main Page',
    'main_slide_1_title_1' => 'years of building facilities for life, work and industry',
    'main_slide_1_title_2' => 'The project portfolio includes more than 600 000 square meters of land area.',
    'main_slide_1_description' => 'The company portfolio includes such well-known objects as “Abay” residential
                                    complex, “Abay 2” residential complex, “Residential complex on Imanov Street,
                                    Avicenna residential complex, Avicenna-Elite” residential complex,  Avicenna 2
                                     residential complex.Class A business centers: “Avicenna-Elite”, “Avicenna No. 1”, “Avicenna No. 2”.',
    'site' => [
        'title' => 'STROINVEST-SK',
        'description' => 'We have been building residential complexes, business centers and industrial facilities since 2003'
    ],
    'advantages' => [
        'title' => 'Our advantages',
        'list' => [
            [
                'title' => 'Industrial base',
                'description' => 'Warehouses, with own concrete-mortar unit, contains and maintains auto-transport and construction equipment'
            ],
            [
                'title' => 'Autopark',
                'description' => 'Road transport, special and heavy construction equipment, units for feeding and transporting concrete mix'
            ],
            [
                'title' => 'Mechanisms and structures for construction',
                'description' => 'Road transport, special and heavy construction equipment, units for feeding and transporting concrete mix'
            ],
            [
                'title' => 'Qualification and experience',
                'description' => 'Leading engineers and trained specialists with construction qualification in the company - from the day of foundation'
            ],

            [
                'title' => 'Operational service',
                'description' => 'Provides sanitary and technical condition of the complex and the adjacent territory, is engaged in landscaping, keeps order and condition of technical equipment, ensuring proper operation'
            ],
            [
                'title' => 'Innovative technologies',
                'description' => 'Energy saving systems, using the “Smart Home” system'
            ],
        ]
    ],
    'objects' => [
        'title' => 'Our objects',
        'button' => 'objects',
        'cat_1' =>  'Residential complex',
        'cat_2' =>  'Industrial sector',
        'desc'  =>  'residential and industrial facilities built',
        'list' => [
            [
                'title' => 'Avicenna',
                'description' => 'Residential apartment complex in the historical center of Astana',
            ],
            [
                'title' => 'Avicenna Elite',
                'description' => 'Administrative residential complex developed by Academician of the International Academy of Architecture Laptev V.A.',
            ],
            [
                'title' => 'Avicenna 2',
                'description' => 'Residential complex with elevated and underground heated parking, using environmentally friendly materials and natural resources',
            ]
        ]
    ],
    'footer' => [
        'social' => 'Our social media network',
        'address' => 'Astana, Saraishyk str 11',
    ],
    'address' => 'Address',
    'phone' => 'Phone',
    'astana' => 'Astana',
    'avicenna' => 'Avicenna',
    'avicenna_2' => 'Avicenna - 2',
    'abay_otyrar' => '«Abay at Otyrar» - Gabdullin',
    'abay_2' => 'Abay - 2',
    'avicenna_elite' => 'Avicenna Elite',
    'imanova' => 'Imanova',
    'about' => [
        'title' => 'About company',
        'short_desc' => '«Stroyinvest-SK» company dates back to 2003, specializing in the construction of
residential complexes with unique memorable architecture',
        'btn_obj' => 'Objects',
        'short_desc_1' => 'Clearly understanding the needs of its customers, the company aims to create
comfortable, functional and high-quality buildings, while focusing not only on
residential sector, but also on business centers and industrial enterprises.',
        'short_desc_2' => 'The project portfolio includes more than 600 000 square meters of land area.',
        'short_desc_3' => 'In 2018, the company implemented a unique project, which is a new mosque in
Astana. It became not only an example of harmonious combination of traditional
standards and modern hi-tech solutions. During the construction of the mosque,
energy-efficient technologies were used that make it possible for the building’s self-
production with the necessary energy.',
        'quote' => 'Today, “Stroyinvest-SK” LLP construction company is a dynamically developing
enterprise with the necessary production and HR potential allowing to reduce the
construction cost price by shortening the time and costs for preparing project
documentation and construction process as a whole. The company is able to put into
practice construction projects of any complexity. Moreover, the basic principles by
which the company is guided in its activity is quality and professionalism, which you
will undoubtedly appreciate!'

    ],
    'projects' => 'Projects',
    'menu' => [
        'main' => 'Main',
        'about' => 'About',
        'objects' => 'Objects',
        'building_mats' => 'Construction materials',
        'news' => 'News',
        'partners' => 'Partners',
        'gallery' => 'Gallery',
        'contacts' => 'Contacts'
    ],

    'direction' => [
        'title' => 'Direction of activity',
        'list' => [
            [
                'title' => 'Residential construction',
                'desc' => 'The first residential facility of the company was built in 2003. Today we have several residential
complex in our portfolio: Abai, Abai 2, Residentil complex on the Imanova street,
Avicenna, Avicenna-Elite, Avicenna 2'
            ],
            [
                'title' => 'Administrative construction',
                'desc' => 'Construction of office buildings, business centers in the city of Astana. The portfolio includes
business centers class A: Avicenna-Elite, Avicenna 1, Avicenna 2.'
            ],
            [
                'title' => 'Industrial engineering',
                'desc' => 'Among industrial projects: Beineu Grain Terminal - built in record time and commissioned in
2010. In 2014, the construction of the Bakery Plant in Petropavlovsk was completed.'
            ],
            [
                'title' => 'Production of construction materials',
                'desc_0' => 'Manufacture and sale of concrete and concrete products',
                'desc_1' => 'Production of metal structures of any complexity',
                'desc_2' => 'Production of exclusive interior joinery products from natural breeds'
            ]
        ]
    ],
    'history' => [
        'title' => 'HISTORY OF THE COMPANY',
        'since' => 'Year of foundation',
        'desc_0' => 'Already in July 2003, the company launched a project to build a 9-storey residential building in the square of Imanova, Internatsionalnaya, Imanbaeva and Koshkarbaeva streets.',
        'desc_1' => 'The area of a residential building is 7203.1 m².',
        'desc_2' => 'In 2004, the company’s team began the construction of a multi-family residential complex “Abai”
in the central part of the city, along Gabdullin Street, consisting of eight blocks and a separate
parking lot. The number of floors of buildings is variable from 9 to 18 floors.',
        'desc_3' => 'The total area of the building is 37,158 m².',
        'desc_4' => 'In 2005, StroyInvest-SK LLP began the implementation of a project for the construction of the
Avicenna residential area, including the residential complex Avicenna, the residential complex
Avicenna-Elite and separate office buildings.',
        'desc_5' => 'In record time, the 2009–2010 year was completed and the Beineu Grain Terminal, a
multifunctional high-tech production complex, was commissioned. The whole technological
process, which includes the reception, storage, processing and shipment of grain, is fully
mechanized and automated.',
        'desc_6' => 'In December 2011, the first stage was put into operation. apartment residential complex Abai-
2, located in the historical center of Astana, in the square of Valikhanov, Otyr and Pasteur
streets. The whole complex consists of seven blocks and a separate parking. The number of
floors of buildings is variable from 14 to 16 floors. The total area of the building is 35,131 m².',
        'desc_7' => 'In 2014, the construction of the Bakery Plant for 120 thousand tons in the city of Petropavlovsk
was completed. High-tech industrial complex includes an integral, logical, fully automated
structure of engineering and technological systems.',
        'desc_8' => 'In 2015-2017, the I and II stage of the multifunctional Residential complex “AVICENNA-2” with
overground and underground parking was commissioned. The total area of the complex is
55,599 m². The project provides for automated systems for effective ventilation and maintaining
a comfortable temperature, as well as energy-efficient technologies.',
        'desc_9' => 'In 2018, the company implemented a unique project - a new mosque in Astana, which became
not only an example of the harmonious combination of traditional standards and modern hi-tech
solutions. During the construction of the mosque, energy-efficient technologies were used that
make it possible for the building to provide itself with necessary energy.',
    ],
    'mats'  =>  [
        'title' =>  'Construction materials',
        'title_1'   =>  'Production of construction materials',
        'desc_0'    =>  'Manufacture and sale of concrete and concrete products',
        'desc_1'    =>  'Sales department',
        'call'      =>  'Submit application',
        'download'  =>  'Download catalogue',
        'desc_2'    =>  'Production of metal structures of any complexity',
        'desc_3'    =>  'Production of exclusive interior joinery products from natural breeds',
    ],

    'partners'  =>  [
        'title' =>  'Partners',
        'trust_us'  =>  'Those who trust us',
        'desc'  =>  '<p class="partners-block__text invest__text">
						Extensive experience in the construction of residential houses and industrial
enterprises, as well as conscientious attitude to its work and execution professionalism
allow “Stroyinvest-SK” company to act as a contractor for international projects.
					</p>	

					<p class="partners-block__text invest__text">
						Such a project is Energy City. It is an innovative, fully autonomous first eco-friendly
town in the world, surrounded by picturesque natural plantations, created by the
project of Weissenseer Austrian Company.
					</p>						

					<p class="partners-block__text invest__text">
						Natural landscapes and modern technologies are harmoniously combined here.
					</p>						

					<p class="partners-block__text invest__text">
						All houses in the town are made of safe and natural materials, using technological
designs that minimize any energy loss. On the roof of each house there are solar
panels that generate natural energy into batteries, which fully provide homes with
electricity and heat, excluding energy costs.
					</p>						

					<p class="partners-block__text invest__text">
						“Smart house” system provides sensors that control the temperature of rooms, lighting
and energy consumption optimization
					</p>'
    ],
    'detail'    =>  'Read more',
    'gallery'   =>  [
        'title' =>  'Gallery',
        'not_photo' =>  'No photo yet',
    ],
    'news'  =>  [
        'title' =>  'News',
        'empty' =>  'news are empty'
    ],
    'contacts'  =>  [
        'title' =>  'Our contacts',
        'address'   =>  'Astana, Saraishyk str. 11',
        'phones'    =>  'Phones'
    ],

    'order' =>  [
        'title' =>  'Call back',
        'name'   =>  'Name',
        'phone' =>  'Phone',
        'submit'    =>  'Submit',
        'comment'   =>  'Сomment'
    ]
];