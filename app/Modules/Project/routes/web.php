<?php

Route::group([
    'prefix' => 'admin',
    'middleware' => [ 'web', 'language'],
    'namespace' => 'App\Modules\Project\Http\Controllers'], function () {

    Route::group(['middleware' => 'guardAuth:admin'], function() {
        Route::resource('project','BackendController');
        Route::delete('project/{id}/remove-images/{image_id}', 'BackendController@removeImages');
    });
});

Route::group([
    'middleware' => [ 'web', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath'],
    'prefix'    =>  \Mcamara\LaravelLocalization\Facades\LaravelLocalization::setLocale(),
    'namespace' => 'App\Modules\Project\Http\Controllers'], function () {
    Route::group([
        'prefix' => 'projects',
    ], function () {
        Route::get('/','FrontendController@index')->name('projects.index');
        Route::get('/{project}', 'FrontendController@show')->name('projects.show');
    });



});