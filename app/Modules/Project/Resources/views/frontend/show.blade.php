@extends('layouts.app')

@section('title', 'Наши объекты')
@section('wrap_class', 'project-inner-block-wrap')

@section('content')
    <div class="conntainer-fluid">
        @include('layouts.partials.header')
    </div>

    <div class="container project-inner-block mar-bot">
        <div class="row">
            @if($project->images->count() > 0)
                <div class="col-md-6 col-sm-12 col-12 project-inner-block__slider">
                    @foreach($project->images as $key=> $projectImage)
                        <img data-image="{{url($projectImage->image->getFullImage())}}" src="{{url($projectImage->image->getFullImage())}}" alt=""
                             class="project-inner-block__image {{$key == 0 ? 'main': null}}">
                    @endforeach
                </div>
            @endif
            <div class="col-md-6 col-sm-12 col-12 project-inner-block__text-wrap">
                <p class="project-inner-block__title">{{$project->title}}</p>

                <div class="project-inner-block__addit">
                    @if($project->city)<p class="project-inner-block__city">{{$project->city}}</p>@endif
                    @if($project->year)<p class="project-inner-block__date">сдан: {{$project->year}}</p>@endif
                </div>

                <div class="project-inner-block__grids">
                    @if($project->sum_of_area)
                        <div class="project-inner-block__secondary">
                            <p class="project-inner-block__secondary-main">{{$project->sum_of_area}}</p>
                            <p class="project-inner-block__secondary-text">Общая площадь здания</p>
                        </div>
                    @endif
                    @if($project->additions)
                        <div class="project-inner-block__secondary">
                            <p class="project-inner-block__secondary-main">{{$project->additions}}</p>
                            <p class="project-inner-block__secondary-text">Составляющие</p>
                        </div>
                    @endif

                    @if($project->floor)
                        <div class="project-inner-block__secondary">
                            <p class="project-inner-block__secondary-main">{{$project->floor}}</p>
                            <p class="project-inner-block__secondary-text">Этажность</p>
                        </div>
                    @endif
                </div>


                <div class="project-inner-block__action-wrap">
                    <a href="/projects" class="project-inner-block__action">Другие объекты
                        <img src="/img/icons/left-arrow.svg" alt="">
                    </a>
                </div>
            </div>
        </div>


        <div class="row about-project">
            <div class="col-md-12 col-sm-12 col-12">
                <p class="about-project__title">О проекте</p>
                <p class="about-project__text">
                    {!! $project->description !!}
                </p>
            </div>
        </div>
    </div>

    <div class="container-fluid footer-container">
        @include('layouts.partials.footer')
    </div>
@endsection	