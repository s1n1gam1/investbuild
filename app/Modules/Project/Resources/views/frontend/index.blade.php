@extends('layouts.app')
@section('title', trans('app.objects.title'))
@section('content')
    <div class="conntainer-fluid">
        @include('layouts.partials.header')
    </div>
    <div class="container mar-bot">
        <div class="row">
            <div class="col-md-12 text-center">
                <p class="main-block__title">{{trans('app.objects.title')}}</p>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-12">
                    <ul class="nav nav-pills mb-3 custom-navs" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">{{trans('app.objects.title')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">
                                <img src="/img/icons/build.png" alt="">
                                {{trans('app.objects.cat_1')}}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">
                                <img src="/img/icons/factory.png" alt="">
                                {{trans('app.objects.cat_2')}}
                            </a>
                        </li>
                    </ul>

                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="container">
                                <div class="row">
                                    <div class="projects-info__row col-md-12 col-sm-12 col-12">

                                        <div class="stat-block">
                                            <p class="stat-block__number">{{$models->count()}}</p>
                                            <p class="stat-block__text">{{trans('app.objects.desc')}}</p>
                                        </div>

                                       @foreach($models as $complex)
                                            <div class="projects-info">
                                                @if($complex->image)
                                                    <img class="projects-info__image" src="{{url($complex->image->getThumb())}}" alt="">
                                                @endif
                                                <div class="projects-info__text-wrap">
                                                    <p class="projects-info__title">{{$complex->title}}</p>
                                                    <p class="projects-info__date">Сдан: {{$complex->year}}</p>
                                                    <div class="projects-info__action-wrap">
                                                        <a href="{{route('projects.show', $complex->id)}}" class="projects-info__action">{{trans('app.detail')}}
                                                            <img src="/img/icons/left-arrow.svg" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                           @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <div class="container">
                                <div class="row">
                                    <div class="projects-info__row col-md-12 col-sm-12 col-12">

                                    @foreach($complexes as $complex)
                                        <div class="projects-info">
                                            @if($complex->image)
                                                <img class="projects-info__image" src="{{url($complex->image->getThumb())}}" alt="">
                                            @endif
                                            <div class="projects-info__text-wrap">
                                                <p class="projects-info__title">{{$complex->title}}</p>
                                                <p class="projects-info__date">Сдан: {{$complex->year}}</p>
                                                <div class="projects-info__action-wrap">
                                                    <a href="{{route('projects.show', $complex->id)}}" class="projects-info__action">{{trans('app.detail')}}
                                                        <img src="/img/icons/left-arrow.svg" alt="">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <div class="container">
                                <div class="row">
                                    <div class="projects-info__row col-md-12 col-sm-12 col-12">
                                    @foreach($industrials as $industrial)
                                        <div class="projects-info">
                                            @if($industrial->image)
                                                <img class="projects-info__image" src="{{url($industrial->image->getThumb())}}" alt="">
                                            @endif
                                            <div class="projects-info__text-wrap">
                                                <p class="projects-info__title">{{$industrial->title}}</p>
                                                <p class="projects-info__date">Сдан: {{$industrial->year}}</p>
                                                <div class="projects-info__action-wrap">
                                                    <a href="{{route('projects.show', $industrial->id)}}" class="projects-info__action">{{trans('app.detail')}}
                                                        <img src="/img/icons/left-arrow.svg" alt="">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>




            </div>



        </div>

    </div>
    <div class="container-fluid footer-container">
        @include('layouts.partials.footer')
    </div>
@endsection