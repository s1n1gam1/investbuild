@extends('admin::layouts.app')
@section('content')
    <div class="box box-body">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('admin::base.add')}} проект</h3>
        </div>

        @include("{$viewPath}.form")
    </div>
@endsection

@section('css')
    <link rel="stylesheet" href="/vendor/admin/datepicker/datepicker3.css">
@endsection

@section('js')
    <script src="/vendor/admin/ckeditor/ckeditor.js"></script>
@endsection
@section('js-append')
    <script>
        CKEDITOR.replace('description_ru');
        CKEDITOR.replace('description_en');
        CKEDITOR.replace('description_kz');
    </script>
@endsection

