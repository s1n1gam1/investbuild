<div class="box-body">
    <div class="form-group">
        <label class="control-label" for="title">Название</label>
        {{Form::text('title_kz',$model->title_kz, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        <label class="control-label" for="desc">Описание</label>
        {{Form::textarea('description_kz',$model->description_kz, ['class' => 'form-control',
        'id' => 'description_kz', 'cols' => 30, 'rows' => 30
        ]) }}
    </div>

    <div class="form-group">
        <label class="control-label" for="city_param">Город</label>
        {{Form::text('city_kz', $model->city_kz, ['class' => 'form-control', 'placeholder' => 'Город'])}}
    </div>

    <div class="form-group">
        <label class="control-label" for="city_param">Год</label>
        {{Form::text('year_kz', $model->year_kz, ['class' => 'form-control', 'placeholder' => 'Год'])}}
    </div>

    <div class="form-group">
        <label class="control-label" for="city_param">Общая площадь</label>
        {{Form::text('sum_of_area_kz', $model->sum_of_area_kz, ['class' => 'form-control', 'placeholder' => 'Общая площадь'])}}
    </div>

    <div class="form-group">
        <label class="control-label" for="city_param">Составляющие</label>
        {{Form::text('additions_kz', $model->additions_kz, ['class' => 'form-control', 'placeholder' => 'Состовляющие'])}}
    </div>

    <div class="form-group">
        <label class="control-label" for="city_param">Этажность</label>
        {{Form::text('floor_kz', $model->floor_kz, ['class' => 'form-control', 'placeholder' => 'Этажность'])}}
    </div>


</div>