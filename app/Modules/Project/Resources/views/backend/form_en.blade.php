<div class="box-body">
    <div class="form-group">
        <label class="control-label" for="title">Название</label>
        {{Form::text('title_en',$model->title_en, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        <label class="control-label" for="desc">Описание</label>
        {{Form::textarea('description_en',$model->description_en, ['class' => 'form-control',
        'id' => 'description_en', 'cols' => 30, 'rows' => 30
        ]) }}
    </div>

    <div class="form-group">
        <label class="control-label" for="city_param">Город</label>
        {{Form::text('city_en', $model->city_en, ['class' => 'form-control', 'placeholder' => 'Город'])}}
    </div>

    <div class="form-group">
        <label class="control-label" for="city_param">Год</label>
        {{Form::text('year_en', $model->year_en, ['class' => 'form-control', 'placeholder' => 'Год'])}}
    </div>

    <div class="form-group">
        <label class="control-label" for="city_param">Общая площадь</label>
        {{Form::text('sum_of_area_en', $model->sum_of_area_en, ['class' => 'form-control', 'placeholder' => 'Общая площадь'])}}
    </div>

    <div class="form-group">
        <label class="control-label" for="city_param">Составляющие</label>
        {{Form::text('additions_en', $model->additions_en, ['class' => 'form-control', 'placeholder' => 'Состовляющие'])}}
    </div>

    <div class="form-group">
        <label class="control-label" for="city_param">Этажность</label>
        {{Form::text('floor_en', $model->floor_en, ['class' => 'form-control', 'placeholder' => 'Этажность'])}}
    </div>


</div>