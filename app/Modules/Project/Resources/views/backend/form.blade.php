<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="box-body">

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if($model->exists)
            {{ Form::open(array('url' => sprintf('/%s/%s',$url, $model->id),'method'=> 'PUT', 'name'=>'update-article', 'files' => true))}}
        @else
            {{ Form::open(array('url' => sprintf('/%s',$url),'method'=> 'POST', 'name'=>'create-article', 'files' => true))}}
        @endif


        <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#ru" aria-controls="ru" role="tab"
                                                          data-toggle="tab">На русском</a></li>
                <li role="presentation"><a href="#en" aria-controls="en" role="tab" data-toggle="tab">На
                        английском</a></li>

                <li role="presentation"><a href="#kz" aria-controls="kz" role="tab" data-toggle="tab">На
                        казахском</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="ru">
                    @include('project::backend.form_ru', compact('model','categories'))
                </div>
                <div role="tabpanel" class="tab-pane" id="en">
                    @include('project::backend.form_en', compact('model','categories'))
                </div>

                <div role="tabpanel" class="tab-pane" id="kz">
                    @include('project::backend.form_kz', compact('model','categories'))
                </div>

                <div class="form-group">
                    <label class="control-label" for="type">Тип</label>
                    {{Form::select('type_id', $model->typeList, $model->type_id, ['class' => 'form-control', 'placeholder' =>
                    'Выберите'])}}
                </div>


                <div class="form-group">
                    <label class="control-label" for="preview">Фото</label>
                    <input type="file" id="image" name="image">

                    @if($model->image)
                        <img src="/{{$model->image->getThumb()}}" alt=""/>
                    @endif

                </div>

                <div class="form-group">
                    <label class="control-label" for="images">Фотографии</label>
                    <input type="file" id="images" name="images[]" multiple="multiple">
                </div>

                @if($model->images)
                    @foreach($model->images as $image)
                        <div class="form-group">
                            <img width="200" src="/{{$image->image->getThumb()}}" alt="" class="thumbnail"/>

                            <button type="button" class="btn btn-warning remove-news-image"
                                    data-action="/admin/project/{{$model->id}}/remove-images/{{$image->id}}"
                                    data-toggle="modal" data-target="#remove-image">
                                <span  class="glyphicon glyphicon-remove"></span> удалить
                            </button>
                        </div>
                    @endforeach
                @endif


                <div class="form-group">
                    <label class="control-label" for="title">{{trans('admin::base.status')}}</label>
                    {{Form::select('status_id', $model->statuses,$model->status_id,
                    ['class'   => 'form-control' ,'placeholder' =>  trans('admin::base.choose')])}}
                </div>
            </div>


            @if($model->exists)
                {{ Form::submit('Обновить', ['class' => 'btn btn-primary']) }}
            @else
                {{ Form::submit('Создать',  ['class' => 'btn btn-primary']) }}
            @endif
            {{ Form::close() }}

            <div>
            </div>
            </div>
        </div>
    </div>
