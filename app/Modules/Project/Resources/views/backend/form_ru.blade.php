<div class="box-body">
    <div class="form-group">
        <label class="control-label" for="title">Название</label>
        {{Form::text('title_ru',$model->title_ru, ['class' => 'form-control']) }}
    </div>


    <div class="form-group">
        <label class="control-label" for="desc">Описание</label>
        {{Form::textarea('description_ru',$model->description_ru, ['class' => 'form-control',
        'id' => 'description_ru', 'cols' => 30, 'rows' => 30
        ]) }}
    </div>


    <div class="form-group">
        <label class="control-label" for="city_param">Город</label>
        {{Form::text('city_ru', $model->city_ru, ['class' => 'form-control', 'placeholder' => 'Город'])}}
    </div>

    <div class="form-group">
        <label class="control-label" for="city_param">Год</label>
        {{Form::text('year_ru', $model->year_ru, ['class' => 'form-control', 'placeholder' => 'Год'])}}
    </div>

    <div class="form-group">
        <label class="control-label" for="city_param">Общая площадь</label>
        {{Form::text('sum_of_area_ru', $model->sum_of_area_ru, ['class' => 'form-control', 'placeholder' => 'Общая площадь'])}}
    </div>

    <div class="form-group">
        <label class="control-label" for="city_param">Составляющие</label>
        {{Form::text('additions_ru', $model->additions_ru, ['class' => 'form-control', 'placeholder' => 'Состовляющие'])}}
    </div>

    <div class="form-group">
        <label class="control-label" for="city_param">Этажность</label>
        {{Form::text('floor_ru', $model->floor_ru, ['class' => 'form-control', 'placeholder' => 'Этажность'])}}
    </div>


</div>