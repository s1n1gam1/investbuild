<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_ru');
            $table->string('title_en')->nullable();
            $table->string('title_kz')->nullable();
            $table->integer('image_id')->nullable();
            $table->text('description_ru');
            $table->text('description_en')->nullable();
            $table->text('description_kz')->nullable();
            $table->string('city_ru')->nullable();
            $table->string('city_en')->nullable();
            $table->string('city_kz')->nullable();
            $table->string('year_ru')->nullable();
            $table->string('year_en')->nullable();
            $table->string('year_kz')->nullable();
            $table->string('sum_of_area_ru')->nullable();
            $table->string('sum_of_area_en')->nullable();
            $table->string('sum_of_area_kz')->nullable();
            $table->string('additions_ru')->nullable();
            $table->string('additions_en')->nullable();
            $table->string('additions_kz')->nullable();
            $table->string('floor_ru')->nullable();
            $table->string('floor_en')->nullable();
            $table->string('floor_kz')->nullable();
            $table->integer('status_id')->default(0);
            $table->integer('type_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
