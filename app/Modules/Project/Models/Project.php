<?php
/**
 * Created by PhpStorm.
 * User: dosar
 * Date: 01.10.2018
 * Time: 18:18
 */

namespace App\Modules\Project\Models;


use App\Modules\Image\Models\Image;
use Dosarkz\Dosmin\Models\I18nModel;

class Project extends I18nModel
{
    const STATUS_DISABLE = 0;
    const STATUS_ACTIVE = 1;
    const TYPE_COMPLEX = 1;
    const TYPE_INDUSTRIAL = 2;

    protected $fillable = [
        'title_ru', 'title_en', 'title_kz', 'image_id', 'description_ru', 'description_en', 'description_kz', 'city_ru',
        'city_en', 'city_kz', 'year_ru', 'year_en', 'year_kz', 'sum_of_area_ru', 'sum_of_area_en', 'sum_of_area_kz',
        'additions_ru', 'additions_en', 'additions_kz', 'floor_ru', 'floor_en', 'floor_kz', 'status_id', 'type_id'
    ];

    public $timestamps = true;

    public function getStatusesAttribute()
    {
        return [
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_DISABLE => 'Не активен'
        ];
    }

    public function listStatuses()
    {
        return [
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_DISABLE => 'Не активен'
        ];
    }

    public function getStatusAttribute()
    {
        return $this->listStatuses()[$this->status_id] ?? null;
    }

    public function image()
    {
        return $this->belongsTo(Image::class,'image_id');
    }

    public function images()
    {
        return $this->hasMany(ProjectImage::class, 'project_id')->with('image')
            ->orderByDesc('created_at');
    }

    public function getTypeListAttribute()
    {
        return [
            self::TYPE_COMPLEX          => 'Жилые комплексы',
            self::TYPE_INDUSTRIAL       => 'Промышленный сектор',
        ];
    }

    public function getTypeAttribute()
    {
        return $this->typeList[$this->type_id];
    }


}