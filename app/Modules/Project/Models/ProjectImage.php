<?php
/**
 * Created by PhpStorm.
 * User: dosar
 * Date: 01.10.2018
 * Time: 18:21
 */

namespace App\Modules\Project\Models;


use App\Modules\Image\Models\Image;
use Illuminate\Database\Eloquent\Model;

class ProjectImage extends Model
{
    const STATUS_ACTIVE = 1;

    protected $fillable = [
        'image_id', 'project_id'
    ];

    public $timestamps = true;

    public function image()
    {
        return $this->belongsTo(Image::class, 'image_id');
    }
}