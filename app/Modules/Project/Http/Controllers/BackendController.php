<?php

namespace App\Modules\Project\Http\Controllers;

use App\Modules\Image\Models\Image;
use App\Modules\Project\Models\Project;
use App\Modules\Project\Models\ProjectImage;
use Dosarkz\Dosmin\Controllers\CrudController;
use Dosarkz\LaravelUploader\BaseUploader;
use Illuminate\Http\Request;

class BackendController extends CrudController
{
    protected $storeValidationRules = [
        'title_ru' => 'required',
        'description_ru'  => 'required',
        'status_id' =>  'required',
        'type_id'   =>  'required'
    ];

    public $afterSaveRedirectUrl = '/admin/project';

    public function __construct()
    {
        $this->setModel(new Project());
        $this->setViewPath('project::backend');
        $this->setFormUrl('admin/project');
        $this->setModule('project');
    }

    protected function beforeValidate(Request $request)
    {
    }

    protected function beforeSave(&$data, $model)
    {
        if (request()->hasFile('image'))
        {
            $file = BaseUploader::image(request()->file('image'),'uploads/news',true, 1024, 768,
                null, 330);

            $image = Image::create([
                'name' => $file->getFileName(),
                'thumb' => $file->getThumb(),
                'path' => $file->getDestination()
            ]);

            $data['image_id'] = $image->id;
        }
    }

    protected function afterSave($model)
    {
        if (request()->hasFile('images')) {
            foreach (request()->file('images') as $item) {
                $uploader = BaseUploader::image($item, 'uploads/projects');
                $image = Image::create([
                    'name' => $uploader->getFileName(),
                    'thumb' => $uploader->getThumb(),
                    'path' => $uploader->getDestination()
                ]);

                ProjectImage::create([
                    'image_id' => $image->id,
                    'project_id' => $model->id,
                    'status_id' => ProjectImage::STATUS_ACTIVE,
                ]);
            }
        }
    }

    public function show($alias)
    {
        $model = $this->getModel()->where('url', $alias)->first();

        if(!$model)
        {
            abort(404);
        }

        $this->setShowParams([
            'model' => $model,
            'url' => $this->getFormUrl(),
            'viewPath' => $this->getViewPath()
        ]);
        return view(sprintf('%s.show', $this->getViewPath()),$this->getShowParams());
    }

    /**
     * @param $page_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeImage($page_id)
    {
        $article = Project::findOrFail($page_id);

        if(!$article->image_id)
        {
            return redirect()->back();
        }

        $image = Image::findOrFail($article->image_id);

        if(file_exists(public_path($image->getThumb())))
        {
            unlink(public_path($image->getThumb()));
        }
        if(file_exists(public_path($image->getFullImage())))
        {
            unlink(public_path($image->getFullImage()));
        }

        $image->delete();

        $article->image_id = Null;
        $article->save();

        return redirect()->back()->with('success', trans('admin::base.photo_deleted'));
    }

    /**
     * @param $imageId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeImages($project_id, $imageId)
    {
        $projectImage = ProjectImage::findOrFail($imageId);

        if ($projectImage) {
            if (file_exists(public_path($projectImage->image->getThumb()))) {
                unlink(public_path($projectImage->image->getThumb()));
            }

            if (file_exists(public_path($projectImage->image->getFullImage()))) {
                unlink(public_path($projectImage->image->getFullImage()));
            }

            $projectImage->image->delete();
            $projectImage->delete();
        }

        return redirect()->back()->with('success', 'Фото успешно удалено');
    }
}