<?php

namespace App\Modules\Project\Http\Controllers;

use App\Modules\Project\Models\Project;
use Dosarkz\Dosmin\Controllers\ModuleController;

class FrontendController extends ModuleController
{
    public function index()
    {
        $models = Project::whereStatusId(Project::STATUS_ACTIVE)->get();
        $complexes = Project::where('type_id', Project::TYPE_COMPLEX)->get();
        $industrials =  Project::where('type_id', Project::TYPE_INDUSTRIAL)->get();

        return view('project::frontend.index', compact('complexes', 'industrials', 'models'));
    }

    public function show(Project $project)
    {
        return view('project::frontend.show', compact('project'));
    }
}
