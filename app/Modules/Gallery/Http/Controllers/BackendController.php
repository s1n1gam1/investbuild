<?php

namespace App\Modules\Gallery\Http\Controllers;

use App\Modules\Gallery\Models\Gallery;
use App\Modules\Gallery\Models\GalleryImage;
use App\Modules\Image\Models\Image;
use Dosarkz\Dosmin\Controllers\CrudController;
use Dosarkz\LaravelUploader\BaseUploader;
use Illuminate\Http\Request;

class BackendController extends CrudController
{
    protected $storeValidationRules = [
        'title_ru' => 'required',
        'image' =>  'required|image',
        'images'    =>  'required',
        'status_id' =>  'required',
    ];

    public $afterSaveRedirectUrl = '/admin/gallery';

    public function __construct()
    {
        $this->setModel(new Gallery());
        $this->setViewPath('gallery::backend');
        $this->setFormUrl('admin/gallery');
        $this->setModule('gallery');
    }

    protected function beforeValidate(Request $request)
    {
    }

    protected function beforeSave(&$data, $model)
    {
        if (request()->hasFile('image'))
        {
            $file = BaseUploader::image(request()->file('image'),'uploads/gallery',true, 1024, 768,
                null, 330);

            $image = Image::create([
                'name' => $file->getFileName(),
                'thumb' => $file->getThumb(),
                'path' => $file->getDestination()
            ]);

            $data['image_id'] = $image->id;
        }
    }

    protected function afterSave($model)
    {
        if (request()->hasFile('images')) {
            foreach (request()->file('images') as $item) {
                $uploader = BaseUploader::image($item, 'uploads/projects');
                $image = Image::create([
                    'name' => $uploader->getFileName(),
                    'thumb' => $uploader->getThumb(),
                    'path' => $uploader->getDestination()
                ]);

                GalleryImage::create([
                    'image_id' => $image->id,
                    'gallery_id' => $model->id,
                ]);
            }
        }
    }

    public function show($alias)
    {
        $model = $this->getModel()->where('url', $alias)->first();

        if(!$model)
        {
            abort(404);
        }

        $this->setShowParams([
            'model' => $model,
            'url' => $this->getFormUrl(),
            'viewPath' => $this->getViewPath()
        ]);
        return view(sprintf('%s.show', $this->getViewPath()),$this->getShowParams());
    }

    /**
     * @param $page_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeImage($page_id)
    {
        $article = Gallery::findOrFail($page_id);

        if(!$article->image_id)
        {
            return redirect()->back();
        }

        $image = Image::findOrFail($article->image_id);

        if(file_exists(public_path($image->getThumb())))
        {
            unlink(public_path($image->getThumb()));
        }
        if(file_exists(public_path($image->getFullImage())))
        {
            unlink(public_path($image->getFullImage()));
        }

        $image->delete();

        $article->image_id = Null;
        $article->save();

        return redirect()->back()->with('success', trans('admin::base.photo_deleted'));
    }

    /**
     * @param $imageId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeImages($project_id, $imageId)
    {
        $projectImage = GalleryImage::findOrFail($imageId);

        if ($projectImage) {
            if (file_exists(public_path($projectImage->image->getThumb()))) {
                unlink(public_path($projectImage->image->getThumb()));
            }

            if (file_exists(public_path($projectImage->image->getFullImage()))) {
                unlink(public_path($projectImage->image->getFullImage()));
            }

            $projectImage->image->delete();
            $projectImage->delete();
        }

        return redirect()->back()->with('success', 'Фото успешно удалено');
    }
}