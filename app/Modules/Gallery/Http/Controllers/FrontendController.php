<?php

namespace App\Modules\Gallery\Http\Controllers;

use App\Modules\Gallery\Models\Gallery;
use Dosarkz\Dosmin\Controllers\ModuleController;

class FrontendController extends ModuleController
{
    public function index()
    {
        $models = Gallery::whereStatusId(Gallery::STATUS_ACTIVE)->paginate();
        return view('gallery::frontend.index', compact('models'));
    }

    public function show(Gallery $gallery)
    {
        $gallery->whereStatusId(Gallery::STATUS_ACTIVE)->firstOrFail();
        return view('gallery::frontend.show', compact('gallery'));
    }
}
