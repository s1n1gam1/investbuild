@extends('layouts.app')

@section('title', trans('app.gallery.title'))

@section('content')
    <div class="conntainer-fluid">
        @include('layouts.partials.header')
    </div>

    <div class="container gallery-block">
        <div class="row">
            <div class="col-md-12 text-center">
                <p class="gallery-block__title">{{trans('app.gallery.title')}}</p>
            </div>
        </div>

        <div class="row">
          @forelse($models as $model)

                <div class="col-md-6 col-sm-6 col-12 gallery-block__item">
                    <div class="gallery-block__item-wrap">
                        @if($model->image)
                        <img class="gallery-block__image img-fluid" src="{{url($model->image->getThumb())}}" alt="">
                        @endif
                        <p class="gallery-block__text">{{$model->title}}</p>
                        <div class="gallery-block__action-wrap">
                            <a href="{{route('gallery.frontend.show', $model->id)}}" class="gallery-block__action">{{trans('app.detail')}}
                                <img src="/img/icons/left-arrow.svg" alt="">
                            </a>
                        </div>
                    </div>
                </div>
              @empty
              <p>{{trans('app.gallery.not_photo')}}</p>
            @endforelse



        </div>
    </div>


    <div class="conntainer-fluid footer-container">
        @include('layouts.partials.footer')
    </div>
@endsection