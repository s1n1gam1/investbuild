<?php

Route::group([
    'prefix' => 'admin',
    'middleware' => [ 'web', 'language'],
    'namespace' => 'App\Modules\Gallery\Http\Controllers'], function () {

    Route::group(['middleware' => 'guardAuth:admin'], function() {
        Route::resource('gallery','BackendController');
        Route::delete('gallery/{id}/remove-images/{image_id}', 'BackendController@removeImages');
    });
});

Route::group([
    'middleware' => [ 'web', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath'],
    'prefix'    =>  \Mcamara\LaravelLocalization\Facades\LaravelLocalization::setLocale(),
    'namespace' => 'App\Modules\Gallery\Http\Controllers'], function () {
    Route::group([
        'prefix' => 'gallery',
    ], function () {
        Route::get('/','FrontendController@index');
        Route::get('/{gallery}','FrontendController@show')->name('gallery.frontend.show');
    });



});