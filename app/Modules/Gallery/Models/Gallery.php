<?php
/**
 * Created by PhpStorm.
 * User: yerzhan
 * Date: 10/5/18
 * Time: 08:19
 */

namespace App\Modules\Gallery\Models;


use App\Modules\Image\Models\Image;
use Dosarkz\Dosmin\Models\I18nModel;

class Gallery extends I18nModel
{
    const STATUS_DISABLE = 0;
    const STATUS_ACTIVE = 1;

    protected $fillable = [
        'title_ru', 'title_kz', 'title_en', 'image_id', 'status_id'
    ];

    public $timestamps = true;


    public function getStatusesAttribute()
    {
        return [
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_DISABLE => 'Не активен'
        ];
    }

    public function listStatuses()
    {
        return [
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_DISABLE => 'Не активен'
        ];
    }

    public function getStatusAttribute()
    {
        return $this->listStatuses()[$this->status_id] ?? null;
    }


    public function image()
    {
        return $this->belongsTo(Image::class,'image_id');
    }

    public function images()
    {
        return $this->hasMany(GalleryImage::class, 'gallery_id')->with('image');
    }
}