<?php
/**
 * Created by PhpStorm.
 * User: yerzhan
 * Date: 10/5/18
 * Time: 08:22
 */

namespace App\Modules\Gallery\Models;


use App\Modules\Image\Models\Image;
use Illuminate\Database\Eloquent\Model;

class GalleryImage extends Model
{
    protected $fillable = [
        'gallery_id', 'image_id'
    ];

    public function image()
    {
        return $this->belongsTo(Image::class, 'image_id');
    }

    public function gallery()
    {
        return $this->belongsTo(Gallery::class,'gallery_id');
    }

}