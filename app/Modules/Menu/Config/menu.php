<?php

return [
    'module' => [
        'alias' => 'menu'
    ],
    'admin' => [
        'model' => \App\Modules\Menu\Models\Menu::class,
    ]
];
