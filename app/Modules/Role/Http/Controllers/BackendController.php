<?php

namespace App\Modules\Role\Http\Controllers;

use App\Modules\Menu\Models\MenuRole;
use App\Modules\Role\Models\RoleModule;
use Dosarkz\Dosmin\Controllers\ModuleController;
use App\Modules\Role\Http\Requests\StoreRoleRequest;
use App\Modules\Role\Http\Requests\UpdateRoleRequest;
use App\Modules\Role\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BackendController extends ModuleController
{
    public function __construct()
    {
        $this->setModule('role');
        $this->setModel(new Role());
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $model = $this->getModel()->paginate();
        $module = $this->getModule();
        return view($this->getModule()->alias . '::backend.index', compact('model', 'module'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $model = $this->getModel();
        $module = $this->getModule();
        return view($this->getModule()->alias . '::backend.create', compact('model', 'module'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(StoreRoleRequest $request)
    {
        $model = $this->getModel()->create($request->all());

        if($request->has('menuRole'))
        {
            foreach ($request->input('menuRole') as  $item) {
                MenuRole::firstOrCreate([
                    'menu_id' => $item,
                    'role_id' => $model->id
                ]);
            }

            foreach ($model->roleMenus as $roleMenu) {
                if (!in_array($roleMenu->menu_id, request()->input('menuRole'))) {
                    $roleMenu->delete();
                }
            }
        }

        if($request->has('roleModules'))
        {
            foreach ($request->input('roleModules') as  $item) {
                RoleModule::firstOrCreate([
                    'module_id' => $item,
                    'role_id' => $model->id
                ]);
            }

            foreach ($model->roleModules as $roleModule) {
                if (!in_array($roleModule->module_id, request()->input('roleModules'))) {
                    $roleModule->delete();
                }
            }
        }




        return redirect('/admin/' . $this->getModule()->alias)
            ->with('success', trans('admin::base.resource_created'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
      //  return view($this->getModule()->alias . '::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $model = $this->getModel()->findOrFail($id);
        $module = $this->getModule();
        return view($this->getModule()->alias . '::backend.edit', compact('model', 'module'));
    }

    /**
     * Update the specified resource in storage.
     * @param UpdateRoleRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRoleRequest $request, $id)
    {
        $model = $this->getModel()->findOrFail($id);

        if($request->has('menuRole'))
        {
            foreach ($request->input('menuRole') as  $item) {
                MenuRole::firstOrCreate([
                    'menu_id' => $item,
                    'role_id' => $model->id
                ]);
            }

            foreach ($model->roleMenus as $roleMenu) {
                if (!in_array($roleMenu->menu_id, request()->input('menuRole'))) {
                    $roleMenu->delete();
                }
            }
        }

        if($request->has('roleModules'))
        {
            foreach ($request->input('roleModules') as  $item) {
                RoleModule::firstOrCreate([
                    'module_id' => $item,
                    'role_id' => $model->id,
                    'status_id' => RoleModule::STATUS_ACTIVE,
                ]);
            }

            foreach ($model->roleModules as $roleModule) {
                if (!in_array($roleModule->module_id, request()->input('roleModules'))) {
                    $roleModule->delete();
                }
            }
        }

        $model->update($request->only('name', 'status_id'));

        return redirect('/admin/' . $this->getModule()->alias)->with('success', trans('admin::base.resource_updated'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $model = $this->getModel()->findOrFail($id);
        $model->delete();
        return redirect()->back()->with('success', trans('admin::base.resource_deleted'));
    }
}