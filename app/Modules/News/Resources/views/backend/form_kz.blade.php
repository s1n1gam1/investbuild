<div class="form-group">
    <label class="control-label" for="title">Заголовок</label>
    {{Form::text('title_kz',$model->title_kz, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    <label class="control-label" for="sdesc">Краткое описание</label>
    {{Form::text('short_description_kz',$model->short_description_kz, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    <label class="control-label" for="desc">Подробное описание</label>
    {{Form::textarea('description_kz',$model->description_kz, ['class' => 'form-control',
    'id' => 'desc_kz', 'cols' => 30, 'rows' => 30
    ]) }}
</div>

<div class="form-group">
    <label class="control-label" for="tags">Ключевые слова</label>
    {{Form::textarea('meta_keywords_kz',$model->meta_keywords_kz, ['class' => 'form-control',
                             'id' => 'desc', 'cols' => 2, 'rows' => 2, 'placeholder' => 'key1,key2,key3'
                             ]) }}
</div>

<div class="form-group">
    <label class="control-label" for="tags">Мета описание</label>
    {{Form::textarea('meta_description_kz',$model->meta_description_kz, ['class' => 'form-control',
                             'id' => 'desc', 'cols' => 2, 'rows' => 2, 'placeholder' => 'text'
                             ]) }}
</div>
