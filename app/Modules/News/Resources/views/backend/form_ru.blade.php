<div class="box-body">
    <div class="form-group">
        <label class="control-label" for="title">Заголовок</label>
        {{Form::text('title_ru',$model->title_ru, ['class' => 'form-control']) }}
    </div>

    <div class="form-group{{ $errors->has('created_at') ? ' has-error' : '' }}">
            <label for="name" class="control-label">Дата</label>

                {{Form::text('created_at', $model->created_at,
                ['class' => 'form-control', 'placeholder' => 'Дата', 'id' => 'datepicker'])}}

                @if ($errors->has('created_at'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('created_at') }}</strong>
                                    </span>
                @endif
    </div>


    <div class="form-group">
        <label class="control-label" for="sdesc">Краткое описание</label>
        {{Form::text('short_description_ru',$model->short_description_ru, ['class' => 'form-control', ]) }}
    </div>

    <div class="form-group">
        <label class="control-label" for="desc">Подробное описание</label>
        {{Form::textarea('description_ru',$model->description_ru, ['class' => 'form-control',
        'id' => 'desc_ru', 'cols' => 30, 'rows' => 30
        ]) }}
    </div>

    <div class="form-group">
        <label class="control-label" for="tags">Ключевые слова</label>
        {{Form::textarea('meta_keywords_ru',$model->meta_keywords_ru, ['class' => 'form-control',
                                 'id' => 'desc', 'cols' => 2, 'rows' => 2, 'placeholder' => 'key1,key2,key3'
                                 ]) }}
    </div>

    <div class="form-group">
        <label class="control-label" for="tags">Мета описание</label>
        {{Form::textarea('meta_description_ru',$model->meta_description_ru, ['class' => 'form-control',
                                 'id' => 'desc', 'cols' => 2, 'rows' => 2, 'placeholder' => 'text'
                                 ]) }}
    </div>


    <div class="form-group">
        <label class="control-label" for="preview">Фото превью</label>
        <input type="file" id="image" name="image">

        @if($model->image)
            <img src="/{{$model->image->getThumb()}}" alt=""/>
        @endif

    </div>

    <div class="form-group">
        <label class="control-label" for="images">Фотографии</label>
        <input type="file" id="images" name="images[]" multiple="multiple">
    </div>

    @if($model->images)
        @foreach($model->images as $image)
            <div class="form-group">
                <img width="200" src="/{{$image->image->getThumb()}}" alt="" class="thumbnail"/>

                <button type="button" class="btn btn-warning remove-news-image"
                        data-action="/admin/news/{{$model->id}}/remove-images/{{$image->id}}"
                        data-toggle="modal" data-target="#remove-image">
                    <span  class="glyphicon glyphicon-remove"></span> удалить
                </button>
            </div>
        @endforeach
    @endif


</div>