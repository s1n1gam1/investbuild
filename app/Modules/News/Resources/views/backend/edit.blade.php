@extends('admin::layouts.app')
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Редактировать новость</h3>
        </div>
        <div class="box-body">
            @include($module->alias.'::backend.form',compact('model'))
        </div>
    </div>

    @include('admin::modals.remove_image_modal')
@endsection


@section('css')
    <link rel="stylesheet" href="/vendor/admin/bootstrap/css/bootstrap-toggle.css">
    <link rel="stylesheet" href="/vendor/admin/datepicker/datepicker3.css">
@endsection

@section('js-append')
    <script src="/vendor/admin/datepicker/bootstrap-datepicker.js"></script>
    <script src="/vendor/admin/datepicker/locales/bootstrap-datepicker.ru.js"></script>
    <script src="/vendor/admin/bootstrap/js/bootstrap-toggle.js"></script>
    <script>
        CKEDITOR.replace('desc_ru',{
            language: 'ru'
        });

        CKEDITOR.replace('desc_en',{
            language: 'ru'
        });

        $(document).ready(function() {
            $('#datepicker').datepicker({
                isRTL: false,
                format: 'yyyy-mm-dd',
                language: 'ru'
            });

            $('#remove-image').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var modal = $(this);

                modal.find('#removeImageForm').attr('action', button.data('action'))
            });


        });
    </script>
@endsection


@section('js')
    <script src="/vendor/admin/ckeditor/ckeditor.js"></script>
@endsection

