<div class="form-group">
    <label class="control-label" for="title">Заголовок</label>
    {{Form::text('title_en',$model->title_en, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    <label class="control-label" for="sdesc">Краткое описание</label>
    {{Form::text('short_description_en',$model->short_description_en, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    <label class="control-label" for="desc">Подробное описание</label>
    {{Form::textarea('description_en',$model->description_en, ['class' => 'form-control',
    'id' => 'desc_en', 'cols' => 30, 'rows' => 30
    ]) }}
</div>

<div class="form-group">
    <label class="control-label" for="tags">Ключевые слова</label>
    {{Form::textarea('meta_keywords_en',$model->meta_keywords_en, ['class' => 'form-control',
                             'id' => 'desc', 'cols' => 2, 'rows' => 2, 'placeholder' => 'key1,key2,key3'
                             ]) }}
</div>

<div class="form-group">
    <label class="control-label" for="tags">Мета описание</label>
    {{Form::textarea('meta_description_en',$model->meta_description_en, ['class' => 'form-control',
                             'id' => 'desc', 'cols' => 2, 'rows' => 2, 'placeholder' => 'text'
                             ]) }}
</div>
