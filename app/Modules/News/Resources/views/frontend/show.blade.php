@extends('layouts.app')

@section('title', $model->title)

@section('og')
    <meta property="og:url" content="{{url('news/'.$model->alias)}}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="{!!  $model->title !!}"/>
    <meta property="og:description" content="{!! strip_tags($model->description) !!}"/>
    @if($model->image)
        <meta property="og:image" content="{{url($model->image->getFullImage())}}"/>
    @endif
@endsection

@section('content')
    <div class="conntainer-fluid">
        @include('layouts.partials.header')
    </div>

    <div class="container mar-bot">
        <div class="row">
            <div class="col-md-12 gallery-detail__left">
                <a href="/news" class="gallery-detail__back">
                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 21.825 21.825" style="enable-background:new 0 0 21.825 21.825;"
                         xml:space="preserve">
					<path style="fill:#ff6600;" d="M16.791,13.254c0.444-0.444,1.143-0.444,1.587,0c0.429,0.444,0.429,1.143,0,1.587l-6.65,6.651
						c-0.206,0.206-0.492,0.333-0.809,0.333c-0.317,0-0.603-0.127-0.81-0.333l-6.65-6.651c-0.444-0.444-0.444-1.143,0-1.587
						s1.143-0.444,1.587,0l4.746,4.762V1.111C9.791,0.492,10.299,0,10.918,0c0.619,0,1.111,0.492,1.111,1.111v16.904L16.791,13.254z"/>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
					</svg>
                    <p>Вернутся назад</p>
                </a>
            </div>

            <div class="col-md-12">
                <p class="invest__text">
                    {{$model->title}}
                </p>
            </div>
            <div class="col-md-12">
                <p class="invest__date">
                    {{$model->created_at->format('d M Y')}}
                </p>
            </div>

            <div class="col-md-10 offset-md-1 text-center">

                @if($model->image)
                    <img class="invest__image" src="{{url($model->image->getFullImage())}}" alt="">
                @else
                    <img class="invest__image" src="/img/gallery8.jpg" alt="">
                @endif


            </div>

            <div class="col-md-10 offset-md-1">
                <p class="invest__fluid-text">
                    {!! $model->description !!}
                </p>
            </div>

            @if($model->images->count() > 0)
                @foreach($model->images as $image)
                    @if($image->image)
                        <img src="{{url($image->image->getFullImage())}}" alt="">
                    @endif
                @endforeach
            @endif
        </div>
    </div>


    <div class="conntainer-fluid footer-container">
        @include('layouts.partials.footer')
    </div>


@endsection
