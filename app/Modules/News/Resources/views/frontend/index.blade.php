@extends('layouts.app')

@section('title', trans('app.news.title'))
@section('content')
    <div class="conntainer-fluid">
        @include('layouts.partials.header')
    </div>

    <div class="container news-block mt-5">
        <div class="row">
            <div class="col-md-12 text-center">
                <p class="news-block__title">{{trans('app.news.title')}}</p>
            </div>
        </div>

        <div class="row">


            @forelse($models as $model)

                <div class="col-md-10 offset-md-1 news-block__item-wrap">
                    @if($model->image)
                        <img class="news-block__item-img" src="{{url($model->image->getThumb())}}" alt="">
                    @else
                        <img class="news-block__item-img" src="/img/gallery8.jpg" alt="">
                    @endif

                    <div class="news-block__right">
                        <p class="news-block__item-title">{{$model->title}}</p>
                        <p class="news-block__item-annotation">{!! $model->short_description !!}</p>
                        <div class="news-block__action-wrap">
                            <a href="/news/{{$model->alias}}" class="news-block__action">{{trans('app.detail')}}
                                <img src="/img/icons/left-arrow.svg" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            @empty
                <p>{{trans('app.news.empty')}}</p>
            @endforelse

            {{$models->links()}}

            {{--<div class="col-md-12">--}}
                {{--<div class="pagination-block">--}}
                    {{--<a href="#" class="pagination-block__item prev">Пред.</a>--}}
                    {{--<a href="#" class="pagination-block__item active">1</a>--}}
                    {{--<a href="#" class="pagination-block__item">2</a>--}}
                    {{--<a href="#" class="pagination-block__item">3</a>--}}
                    {{--<a href="#" class="pagination-block__item next">След.</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>

    <div class="conntainer-fluid footer-container">
        @include('layouts.partials.footer')
    </div>
@endsection
