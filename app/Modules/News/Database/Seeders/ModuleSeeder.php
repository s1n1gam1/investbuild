<?php

namespace App\Modules\News\Database\Seeders;

use Dosarkz\Dosmin\Models\Module;
use App\Modules\Menu\Models\Menu;
use App\Modules\Menu\Models\MenuItem;
use App\Modules\Menu\Models\MenuRole;
use App\Modules\Role\Models\Role;
use Illuminate\Database\Seeder;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if ($menu = Menu::where('alias', 'news')->first()) {
            $menu->menuItems()->delete();
            $menu->menuRoles()->delete();
            $menu->delete();
        }

        $module = Module::firstOrCreate([
            'alias' => lcfirst('News'),
        ]);

        $module->update([
            'name_ru' => 'News',
            'name_en' => 'News',
            'menu_active' => true,
            'description_ru' => 'News',
            'description_en' => 'News',
            'version' => 0.01,
            'status_id' => Module::STATUS_ACTIVE,
        ]);

        $menu = Menu::create([
            'name_en' => 'News',
            'name_ru' => 'Новости',
            'alias' => 'news',
            'type_id' => Menu::TYPE_LEFT_SIDE_MENU,
            'module_id' => $module->id,
            'status_id' => 1,
            'position' => 2,
        ]);

        $MenuItem = MenuItem::create([
            'title_ru' => 'Новости',
            'title_en' => 'News',
            'url' => '/admin/news',
            'icon' => 'fa-newspaper',
            'position' => 1,
            'menu_id' => $menu->id,
            'status_id' => 1
        ]);

        MenuItem::create([
            'title_ru' => 'Добавить',
            'title_en' => 'Add',
            'url' => '/admin/news/create',
            'icon' => 'fa-plus-circle',
            'menu_id' => $menu->id,
            'parent_id' => $MenuItem->id,
            'position' => 1,
            'status_id' => 1
        ]);

        MenuItem::create([
            'title_ru' => 'Список',
            'title_en' => 'List',
            'url' => '/admin/news',
            'icon' => 'fa-list-ul',
            'menu_id' => $menu->id,
            'parent_id' => $MenuItem->id,
            'position' => 1,
            'status_id' => 1
        ]);

        MenuRole::create([
            'role_id' => Role::where('alias', 'admin')->first()->id,
            'menu_id' => $menu->id,
        ]);

    }
}
