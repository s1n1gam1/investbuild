<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_ru');
            $table->string('title_en')->nullable();
            $table->string('title_kz')->nullable();
            $table->string('alias')->unique();
            $table->text('short_description_ru');
            $table->text('short_description_en')->nullable();
            $table->text('short_description_kz')->nullable();
            $table->text('description_ru')->nullable();
            $table->text('description_en')->nullable();
            $table->text('description_kz')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('image_id')->nullable();
            $table->integer('views')->unsigned()->default(0);
            $table->integer('category_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('meta_description_ru')->nullable();
            $table->string('meta_keywords_ru')->nullable();
            $table->string('meta_description_kz')->nullable();
            $table->string('meta_keywords_kz')->nullable();
            $table->string('meta_description_en')->nullable();
            $table->string('meta_keywords_en')->nullable();
            $table->integer('status_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
