<?php

Route::group([
    'prefix' => 'admin',
    'middleware' => [ 'web', 'language'],
    'namespace' => 'App\Modules\News\Http\Controllers'], function () {

    Route::group(['middleware' => 'guardAuth:admin'], function() {
        Route::resource('news','BackendController');
        Route::delete('news/{id}/remove-images/{image_id}', 'BackendController@removeImages');
    });
});

Route::group([
    'middleware' => [ 'web', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath'],
    'prefix'    =>  \Mcamara\LaravelLocalization\Facades\LaravelLocalization::setLocale(),
    'namespace' => 'App\Modules\News\Http\Controllers'], function () {
    Route::group([
        'prefix' => 'news',
    ], function () {
        Route::get('/','FrontendController@index');
        Route::get('/{id}', 'FrontendController@show');
    });
});