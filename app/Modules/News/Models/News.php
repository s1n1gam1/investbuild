<?php

namespace App\Modules\News\Models;

use App\Modules\Image\Models\Image;
use Dosarkz\Dosmin\Models\I18nModel;

class News extends I18nModel
{
    const STATUS_CREATED = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_DEACTIVATED  = 3;
    const STATUS_SEND_TO_MODERATE = 4;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'news';
    protected $fillable = ['title_ru','title_en', 'title_kz', 'alias', 'short_description_ru', 'short_description_en',
        'short_description_kz','description_ru', 'description_kz', 'description_en', 'image_id', 'views', 'category_id',
        'meta_description_ru','meta_description_kz', 'meta_keywords_kz',
        'meta_description_en','meta_keywords_ru','meta_keywords_en','created_at', 'status_id'];


    public function getCreatedDateMainAttribute()
    {
        Date::setLocale('ru');
        return Date::parse($this->created_at)->format('d.m.Y');
    }

    public function getCreatedDateAttribute()
    {
        Date::setLocale('ru');
        return Date::parse($this->created_at)->format('j F Y');
    }

    public function image()
    {
        return $this->belongsTo(Image::class,'image_id');
    }

    public function listStatuses()
    {
        return [
            self::STATUS_CREATED => 'Создан',
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_DEACTIVATED => 'Не активен',
            self::STATUS_SEND_TO_MODERATE => 'На модерации'
        ];
    }

    public function getStatusesAttribute()
    {
        return [

            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_DEACTIVATED => 'Не активен'
        ];
    }

    public function getStatusAttribute()
    {
        return $this->listStatuses()[$this->status_id] ?? null;
    }

    public function images()
    {
        return $this->hasMany(NewsImage::class, 'news_id')->with('image');
    }

}
