<?php

namespace App\Modules\News\Http\Requests;

use App\Modules\News\Models\News;
use Illuminate\Foundation\Http\FormRequest;

class UpdateNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules  = [
            'title_ru' => 'required|string',
            'short_description_ru' => 'required|string',
            'description_ru'   => 'required|string',
            'image' =>  'image',
            'status_id' =>  'required'
        ];

        return $rules;
    }
}
