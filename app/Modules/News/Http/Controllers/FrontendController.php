<?php

namespace App\Modules\News\Http\Controllers;

use App\Modules\News\Models\News;
use Dosarkz\Dosmin\Controllers\ModuleController;

class FrontendController extends ModuleController
{
    public function __construct()
    {
        $this->setModule('news');
        $this->setModel(new News());
    }

    public function index()
    {
        $models = News::where('status_id',News::STATUS_ACTIVE)->orderBy('created_at','desc')->paginate();
        return view('news::frontend.index', compact('models'));
    }

    public function show($alias)
    {
        $model =  News::whereAlias($alias)->where('status_id',News::STATUS_ACTIVE)->first();

        if(!$model)
            abort(404);

        return view('news::frontend.show', compact('model'));
    }

}
