<?php

namespace App\Modules\News\Http\Controllers;

use App\Modules\News\Http\Requests\StoreNewsRequest;
use App\Modules\News\Http\Requests\UpdateNewsRequest;
use App\Modules\News\Models\News;
use App\Modules\News\Models\NewsImage;
use Dosarkz\Dosmin\Controllers\ModuleController;
use App\Modules\Image\Models\Image;
use Dosarkz\LaravelUploader\BaseUploader;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;

class BackendController extends ModuleController
{
    public function __construct()
    {
        $this->setModule('news');
        $this->setModel(new News());
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $model = $this->getModel()
            ->orderBy('id', 'desc');

        $model = $model->paginate();
        $module = $this->getModule();

        return view($this->getModule()->alias . '::backend.index', compact('model', 'module'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $model = $this->getModel();
        $model->created_at = date('Y-m-d');
        $module = $this->getModule();
        return view($this->getModule()->alias . '::backend.create', compact('model', 'module'));
    }

    /**
     * @param StoreNewsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreNewsRequest $request)
    {
        $request->merge([
            'user_id' => auth()->guard('admin')->user()->id,
            'alias' => trans_url($request->input('title_ru')),
        ]);

        $model = $this->getModel()->create($request->all());

        if ($request->hasFile('image'))
        {
            $file = BaseUploader::image($request->file('image'),'uploads/news',true, 1024, 768,
                null, 330);

            $image = Image::create([
                'name' => $file->getFileName(),
                'thumb' => $file->getThumb(),
                'path' => $file->getDestination()
            ]);

            $model->image_id = $image->id;
            $model->save();
        }

        if (request()->hasFile('images')) {
            foreach (request()->file('images') as $item) {
                $uploader = BaseUploader::image($item, 'uploads/news');
                $image = Image::create([
                    'name' => $uploader->getFileName(),
                    'thumb' => $uploader->getThumb(),
                    'path' => $uploader->getDestination()
                ]);

                NewsImage::create([
                    'image_id' => $image->id,
                    'news_id' => $model->id,
                    'status_id' => NewsImage::STATUS_ACTIVE,
                ]);
            }
        }

        return redirect('/admin/' . $this->getModule()->alias)->with('success', "Новость успешно создана");
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view($this->getModule()->alias . '::backend.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $model = $this->getModel()->findOrFail($id);
        $module = $this->getModule();
        return view($this->getModule()->alias . '::backend.edit', compact('model', 'module'));
    }


    public function update(UpdateNewsRequest $request, $id)
    {
        $model = $this->getModel()->findOrFail($id);

        if ($request->hasFile('image'))
        {
            $file = BaseUploader::image($request->file('image'),'uploads/news',true, 1024, 768,
                null, 330);

            $image = Image::create([
                'name' => $file->getFileName(),
                'thumb' => $file->getThumb(),
                'path' => $file->getDestination()
            ]);

            $model->image_id = $image->id;
            $model->save();
        }

        if (request()->hasFile('images')) {
            foreach (request()->file('images') as $item) {
                $uploader = BaseUploader::image($item, 'uploads/news');
                $image = Image::create([
                    'name' => $uploader->getFileName(),
                    'thumb' => $uploader->getThumb(),
                    'path' => $uploader->getDestination()
                ]);

                NewsImage::create([
                    'image_id' => $image->id,
                    'news_id' => $model->id,
                    'status_id' => NewsImage::STATUS_ACTIVE,
                ]);
            }
        }

        $status = $request->input('status_id');

        if(!isset($status))
        {
            $request->merge([
                'status_id' => $model->status_id
            ]);
        }

        $model->update($request->all());

        return redirect('/admin/' . $this->getModule()->alias)->with('success', 'success');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $model = $this->getModel()->findOrFail($id);
        $model->delete();
        return redirect()->back()->with('success', 'deleted');
    }

    /**
     * @param $tour_id
     * @param $imageId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeImages($news_id, $imageId)
    {
        $tour_image = NewsImage::findOrFail($imageId);

        if ($tour_image) {
            if (file_exists(public_path($tour_image->image->getThumb()))) {
                unlink(public_path($tour_image->image->getThumb()));
            }

            if (file_exists(public_path($tour_image->image->getFullImage()))) {
                unlink(public_path($tour_image->image->getFullImage()));
            }

            $tour_image->image->delete();
            $tour_image->delete();
        }

        return redirect()->back()->with('success', 'Фото успешно удалено');
    }

}
