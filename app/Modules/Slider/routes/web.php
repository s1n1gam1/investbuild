<?php

Route::group([
    'prefix' => 'admin',
    'middleware' => [ 'web', 'language'],
    'namespace' => 'App\Modules\Slider\Http\Controllers'], function () {

    Route::group(['middleware' => 'guardAuth:admin'], function() {
        Route::resource('slider','BackendController');
    });
});

Route::group([
    'middleware' => [ 'web'],
    'namespace' => 'App\Modules\Slider\Http\Controllers'], function () {
    Route::group([
        'prefix' => 'slider',
    ], function () {
        Route::get('/','FrontendController@index');
    });



});