<?php
/**
 * Created by PhpStorm.
 * User: yerzhan
 * Date: 10/5/18
 * Time: 09:24
 */

namespace App\Modules\Slider\Models;


use App\Modules\Image\Models\Image;
use Dosarkz\Dosmin\Models\I18nModel;

class Slider extends I18nModel
{

    const STATUS_DISABLE = 0;
    const STATUS_ACTIVE = 1;

    protected $fillable = [
        'image_id', 'title_ru', 'title_en', 'title_kz', 'short_desc_ru', 'short_desc_en', 'short_desc_kz', 'btn_label_ru',
        'btn_label_en', 'btn_label_kz', 'btn_url', 'status_id'
    ];

    public $timestamps = true;


    public function getStatusesAttribute()
    {
        return [
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_DISABLE => 'Не активен'
        ];
    }

    public function listStatuses()
    {
        return [
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_DISABLE => 'Не активен'
        ];
    }

    public function getStatusAttribute()
    {
        return $this->listStatuses()[$this->status_id] ?? null;
    }


    public function image()
    {
        return $this->belongsTo(Image::class,'image_id');
    }
}