<div class="box-body">
    <div class="form-group">
        <label class="control-label" for="title">Название</label>
        {{Form::text('title_en',$model->title_en, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        <label class="control-label" for="title">Короткое описание</label>
        {{Form::textarea('short_desc_en',$model->short_desc_en, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        <label class="control-label" for="title">Название кнопки</label>
        {{Form::text('btn_label_en',$model->btn_label_en, ['class' => 'form-control']) }}
    </div>

</div>