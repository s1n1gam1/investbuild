<div class="box-body">
    <div class="form-group">
        <label class="control-label" for="title">Название</label>
        {{Form::text('title_kz',$model->title_kz, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        <label class="control-label" for="title">Короткое описание</label>
        {{Form::textarea('short_desc_kz',$model->short_desc_kz, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        <label class="control-label" for="title">Название кнопки</label>
        {{Form::text('btn_label_kz',$model->btn_label_kz, ['class' => 'form-control']) }}
    </div>


</div>