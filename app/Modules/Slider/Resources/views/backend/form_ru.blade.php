<div class="box-body">
    <div class="form-group">
        <label class="control-label" for="title">Название</label>
        {{Form::text('title_ru',$model->title_ru, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        <label class="control-label" for="title">Короткое описание</label>
        {{Form::textarea('short_desc_ru',$model->short_desc_ru, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        <label class="control-label" for="title">Название кнопки</label>
        {{Form::text('btn_label_ru',$model->btn_label_ru, ['class' => 'form-control']) }}
    </div>


</div>