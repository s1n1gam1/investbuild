<?php

namespace App\Modules\Slider\Http\Controllers;

use App\Modules\Image\Models\Image;
use App\Modules\Slider\Models\Slider;
use Dosarkz\Dosmin\Controllers\CrudController;
use Dosarkz\LaravelUploader\BaseUploader;
use Illuminate\Http\Request;

class BackendController extends CrudController
{
    protected $storeValidationRules = [
        'title_ru' => 'required',
        'image' =>  'required|image',
        'short_desc_ru' =>  'required|max:255',
        'short_desc_en' =>  'max:255',
        'short_desc_kz' =>  'max:255',
        'status_id' =>  'required',
    ];

    public $afterSaveRedirectUrl = '/admin/slider';

    public function __construct()
    {
        $this->setModel(new Slider());
        $this->setViewPath('slider::backend');
        $this->setFormUrl('admin/slider');
        $this->setModule('slider');
    }

    protected function beforeValidate(Request $request)
    {
    }

    protected function beforeSave(&$data, $model)
    {
        if (request()->hasFile('image'))
        {
            $file = BaseUploader::image(request()->file('image'),'uploads/gallery',true, 1024, 768,
                null, 330);

            $image = Image::create([
                'name' => $file->getFileName(),
                'thumb' => $file->getThumb(),
                'path' => $file->getDestination()
            ]);

            $data['image_id'] = $image->id;
        }
    }


    /**
     * @param $page_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeImage($page_id)
    {
        $article = Slider::findOrFail($page_id);

        if(!$article->image_id)
        {
            return redirect()->back();
        }

        $image = Image::findOrFail($article->image_id);

        if(file_exists(public_path($image->getThumb())))
        {
            unlink(public_path($image->getThumb()));
        }
        if(file_exists(public_path($image->getFullImage())))
        {
            unlink(public_path($image->getFullImage()));
        }

        $image->delete();

        $article->image_id = Null;
        $article->save();

        return redirect()->back()->with('success', trans('admin::base.photo_deleted'));
    }

}