<?php

namespace App\Modules\Slider\Http\Controllers;

use App\Modules\Slider\Models\Slider;
use Dosarkz\Dosmin\Controllers\ModuleController;

class FrontendController extends ModuleController
{
   // public function __construct()
 //   {
  //      $this->setModule('slider');
  //      $this->setModel(new Slider());
  //  }

    public function index()
    {
        return view('slider::frontend.index');
    }
}
