<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('image_id');
            $table->string('title_ru');
            $table->string('title_en')->nullable();
            $table->string('title_kz')->nullable();
            $table->string('short_desc_ru');
            $table->string('short_desc_en')->nullable();
            $table->string('short_desc_kz')->nullable();
            $table->string('btn_label_ru')->nullable();
            $table->string('btn_label_en')->nullable();
            $table->string('btn_label_kz')->nullable();
            $table->string('btn_url')->nullable();
            $table->integer('status_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sliders');
    }
}
