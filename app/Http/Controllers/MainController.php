<?php
/**
 * Created by PhpStorm.
 * User: dosar
 * Date: 18.10.2018
 * Time: 11:34
 */

namespace App\Http\Controllers;


use App\Http\Requests\FeedbackRequest;
use App\Mail\FeedbackMail;
use Illuminate\Support\Facades\Mail;

class MainController extends Controller
{
    public function feedback(FeedbackRequest $request)
    {
          Mail::to(env('MAIL_RECEIVER'))->send(new FeedbackMail($request->all()));
          return redirect()->back()->with('success', 'Ваше письмо успешно отправлено');
    }
}