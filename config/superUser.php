<?php

return [
    'module' => [
        'alias' => 'superUser'
    ],
    'admin' => [
        'model' => App\Modules\SuperUser\Models\SuperUser::class,
    ]
];
