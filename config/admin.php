<?php

return [
    'debug' =>  false,
    'name' => 'Dosmin',
    'auth' => [
        'guards' => [
            'web' => [
                'driver' => 'session',
                'provider' => 'users',
            ],
            'api' => [
                'driver' => 'token',
                'provider' => 'users',
            ],
            'admin' => [
                'driver'   => 'session',
                'provider' => 'admin',
            ],
        ],
        'providers' => [
            'admin' => [
                'driver' => 'eloquent',
                'model'  => App\Modules\SuperUser\Models\SuperUser::class,
            ],
            'users' => [
                'driver' => 'eloquent',
                'model' => App\User::class,
            ],
        ],
    ],

    'modules' => [
        'cache' => [
            'enabled' => false,
            'key' => 'laravel-modules',
            'lifetime' => 60,
        ],
        'providers' => [
            'menu' => App\Modules\Menu\Providers\MenuServiceProvider::class,
            'superUser' => App\Modules\SuperUser\Providers\SuperUserServiceProvider::class,
            'image' => App\Modules\Image\Providers\ImageServiceProvider::class,
            'role' => App\Modules\Role\Providers\RoleServiceProvider::class,
            'article' => App\Modules\Article\Providers\ArticleServiceProvider::class,
            'news'  =>  \App\Modules\News\Providers\NewsServiceProvider::class,
            'project' =>  \App\Modules\Project\Providers\ProjectServiceProvider::class,
            'gallery' =>  \App\Modules\Gallery\Providers\GalleryServiceProvider::class,
            'slider' =>  \App\Modules\Slider\Providers\SliderServiceProvider::class
        ]
    ]

];