<?php

return [
    'module' => [
        'alias' => 'image'
    ],
    'admin' => [
        'model' => \App\Modules\Image\Models\Image::class,
    ]
];
